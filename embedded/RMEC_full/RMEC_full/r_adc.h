/*
 * r_adc.h
 *
 * Created: 10/28/2018 5:09:06 PM
 *  Author: Ruben Castro
 */ 

 #ifndef R_ADC_H_
 #define R_ADC_H_

 typedef struct{
	ADC_t *adc;
	ADC_CH_t *ch;

 }r_adc_t;

 void r_adc_init(r_adc_t *r_adc, ADC_t *adcv, ADC_CH_t *ch, ADC_CH_MUXPOS_t posPin);
 uint16_t r_adc_scan(r_adc_t *r_adc);

 #endif /* R_ADC_H_ */