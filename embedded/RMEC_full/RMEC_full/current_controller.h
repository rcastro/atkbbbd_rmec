/*
 * current_controller.h
 *
 * Created: 10/16/2018 7:19:50 PM
 *  Author: Ruben Castro
 */ 


#ifndef CURRENT_CONTROLLER_H_
#define CURRENT_CONTROLLER_H_


typedef struct{
	double P;
	double I;
	double dt;
	int MAX_IN;
	int MIN_IN;
	int MAX_OUT;
	int MIN_OUT;

	double prevError;
	double currentError;
	double totalError ;

	double maxtotalError;
	double minTotalError;

	double setpoint;
	double acceptableRange;
	int onTargetTicks;
	

}currentc_t;

void currentc_init(currentc_t *cc, double P, double I, double dt, int MIN_IN, int MAX_IN, int MIN_OUT, int MAX_OUT);
double currentc_crunch(currentc_t *pid, double input);
void currentc_setSetpoint(currentc_t *pid, double val);
void currentc_setAcceptableRange(currentc_t *pid, double range);
uint8_t currentc_onTarget(currentc_t *pid);




#endif /* CURRENT_CONTROLLER_H_ */