/*
 * IncFile1.h
 *
 * Created: 10/7/2018 1:47:25 PM
 *  Author: raque
 */ 


#ifndef RBasic_H_
#define RBasic_H_

#define CONSTRAIN(a, min,max) (a<min? min: a>max? max: a)
#define MAP(a,in_min, in_max, out_min, out_max) ((a-in_min)*(out_max-out_min/in_max-in_min) + out_min)



#endif /* INCFILE1_H_ */