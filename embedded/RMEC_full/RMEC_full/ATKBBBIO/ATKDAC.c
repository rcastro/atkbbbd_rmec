/*
* ATKDAC.c
*
* Created: 10/7/2018 2:00:13 PM
*  Author: Ruben Castro
*/
#include <avr/io.h>
#include "ATKDAC.h"

void dac_begin( ATK_DAC_CHANNEL_sel channel){
	
	DACB.CTRLC = DAC_REFSEL_AVCC_gc;
	if(channel == ATK_DAC_DUAL_CHANNEL){
		DACB.CTRLB = DAC_CHSEL_DUAL_gc;
	}else{
		DACB.CTRLB = DAC_CHSEL_SINGLE_gc;
	}

	if(channel & ATK_DAC_CHANNEL_0 || channel & ATK_DAC_DUAL_CHANNEL){
		DACB.CTRLA |= DAC_CH0EN_bm | DAC_ENABLE_bm; //enable output pin 0 - PB2
		set_dac(ATK_DAC_CHANNEL_0, 0);
	}
	if(channel & ATK_DAC_CHANNEL_1 || channel & ATK_DAC_DUAL_CHANNEL){
		DACB.CTRLA |= DAC_CH1EN_bm | DAC_ENABLE_bm; //enable output pin 1 - PB3
		set_dac(ATK_DAC_CHANNEL_1, 0);
	}


}

void set_dac( ATK_DAC_CHANNEL_sel channel, uint16_t val){ //set 12 bit dac value
	
	if(channel & ATK_DAC_CHANNEL_0){
		while(!(DACB.STATUS & DAC_CH0DRE_bm));
		DACB.CH0DATA = val;
	}
	if(channel & ATK_DAC_CHANNEL_1){
		while(!(DACB.STATUS & DAC_CH1DRE_bm));
		DACB.CH1DATA = val;
	}


}