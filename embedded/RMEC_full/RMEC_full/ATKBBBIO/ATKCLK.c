/*
 * ATKCLK.c
 *
 * Created: 10/7/2018 2:00:48 PM
 *  Author: Jake Read and Ruben Castro
 */ 
 #include <avr/io.h>
 #include "ATKCLK.h"


 void clock_init(void){
	 OSC.XOSCCTRL = OSC_XOSCSEL_XTAL_256CLK_gc | OSC_FRQRANGE_12TO16_gc; // select external source
	 OSC.CTRL = OSC_XOSCEN_bm; // enable external source
	 while(!(OSC.STATUS & OSC_XOSCRDY_bm)); // wait for external
	 OSC.PLLCTRL = OSC_PLLSRC_XOSC_gc | OSC_PLLFAC0_bm | OSC_PLLFAC1_bm; // select external osc for pll, do pll = source * 3
	 //OSC.PLLCTRL = OSC_PLLSRC_XOSC_gc | OSC_PLLFAC1_bm; // pll = source * 2 for 32MHz std clock
	 OSC.CTRL |= OSC_PLLEN_bm; // enable PLL
	 while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
	 CCP = CCP_IOREG_gc; // enable protected register change
	 CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL for main clock
 }