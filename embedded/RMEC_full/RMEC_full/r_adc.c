/*
 * r_adc.c
 *
 * Created: 10/28/2018 5:08:55 PM
 *  Author: Ruben Castro
 */ 

 #include <avr/io.h>
 #include "RBasic.h"
 #include "r_adc.h"

 void r_adc_init(r_adc_t *r_adc, ADC_t *adc, ADC_CH_t *ch, ADC_CH_MUXPOS_t posPin){
	 r_adc->adc=adc;
	 r_adc->ch=ch;

	 //setup ADC
	 adc->CTRLA= 1<<0;//enable ADC
	 adc->CTRLB= ADC_RESOLUTION_12BIT_gc | ADC_FREERUN_bm; 
	 adc->REFCTRL= ADC_REFSEL_INTVCC_gc;
	 adc->PRESCALER= ADC_PRESCALER_DIV512_gc;

	 //setup ADCChannel
	 PORTB.DIRCLR |= PIN0_bm;
	 ch->CTRL=ADC_CH_INPUTMODE_DIFFWGAIN_gc | ADC_CH_GAIN_DIV2_gc;
	 ch->MUXCTRL = ADC_CH_MUXNEG_GND_MODE4_gc | posPin; 
		//ch->CTRL = ADC
			
 }

 uint16_t r_adc_scan(r_adc_t *r_adc){
	//r_adc->ch->CTRL |= 1<<7; //start scan
	//while(r_adc->ch->CTRL & (1<<7));//wait for scan to end
	return CONSTRAIN( ((r_adc->ch->RESH<<8) + r_adc->ch->RESL -2018)*1.35 , 1,4095); 
 }