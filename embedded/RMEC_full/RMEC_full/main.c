/*
 * RMEC_full.c
 *
 * Created: 11/29/2018 1:01:18 AM
 * Author : Ruben Castro
 */ 

#include <avr/io.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include "RPID.h"
#include "pin.h"
#include "ATKBBBIO/ATKDAC.h"
#include "ATKBBBIO/ATKCLK.h"
#include "r_adc.h"
#include "motor.h"
#include <stdlib.h>
#include "pwm.h"
#include "RBasic.h"
#include "current_controller.h"
#include "ATKBBBIO/ATKEncoder.h"

pwm_t PWM1;//H bridge pwm val
pwm_t PWM2;//H bridge pwm val

r_adc_t ADC_CS;//current sensing ADC

currentc_t CC;




void timing_interrupts_init(){
	TCC1.CTRLA = TC_CLKSEL_DIV1_gc;

	uint16_t pera = 24000; //  2khz (48MHZ/24000)
	uint8_t peral = (uint8_t) pera;
	uint8_t perah = (uint8_t) (pera >> 8);

	TCC1.PERBUFL = peral;
	TCC1.PERBUFH = perah;

	TCC1.INTCTRLA = TC_OVFINTLVL_MED_gc;
}


int i=0;

ISR(TCC1_OVF_vect){
	i++;
	if(!(i%1000)){ //blink per second
		PORTE_DIRTGL = PIN2_bm;
	}
	//double motorVal = (i/20)%100;
	//pwm_set(&PWM1, motorVal);
	//
	pwm_set_dutycycle(&PWM1,40);
	pwm_set_dutycycle(&PWM2, 0);

}



int main(void)
{
	/* Replace with your application code */
	
	//initialize Clock to 48 Mhz
	clock_init();

	//initialize interrupts
	sei();
	PMIC.CTRL |= PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;

	PORTE.DIRSET |= PIN3_bm | PIN2_bm;


	
	//start DAC, PWM, and Encoder
	PORTB.DIRSET |= PIN2_bm | PIN3_bm;
	dac_begin(ATK_DAC_DUAL_CHANNEL);
	ATKEncoder_begin(1);//1 tick/unit

	
	//initialize motor H bridge pins
	PORTC.DIRSET |= PIN0_bm | PIN1_bm;
	pwm_init(&PWM1, &TCC0,TC_CLKSEL_DIV1_gc,TC0_CCAEN_bm,48000,20);
	pwm_init(&PWM2, &TCC0,TC_CLKSEL_DIV1_gc,TC0_CCBEN_bm,48000,20);

	//initialize current sense ADC
	PORTB_DIRCLR |= PIN0_bm;
	r_adc_init(&ADC_CS, &ADCB, &ADCB.CH0, ADC_CH_MUXPOS_PIN0_gc);

	
	PORTE_DIRSET = PIN2_bm | PIN3_bm;

	//initialize RMEC by making nSleep high
	PORTC.DIRSET |= PIN3_bm;
	PORTC.OUTSET |= PIN3_bm;


	timing_interrupts_init();
	while(1){
		
	}

}