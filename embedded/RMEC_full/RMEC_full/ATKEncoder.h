/*
 * ATKEncoder.h
 *
 * Created: 10/8/2018 1:45:06 PM
 *  Author: raque
 */ 


#ifndef ATKENCODER_H_
#define ATKENCODER_H_

void ATKEncoder_begin(double tickPerV);

int ATK_EN_getCounter();

double ATK_EN_getValue();

 ISR(PORTC_INT0_vect);
  ISR(PORTC_INT1_vect);


#endif /* ATKENCODER_H_ */