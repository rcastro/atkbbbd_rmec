/*
 * ATKPWM0.h
 *
 * Created: 10/7/2018 3:03:49 PM
 *  Author: Ruben Castro
 */ 


#ifndef PWM_H_
#define PWM_H_

typedef struct{
	TC0_t *timer;
	TC_CLKSEL_t prescalar;
	uint8_t pwm_channel;
	int base_frequency;
	int desired_frequency;
	int per;
}pwm_t;



 void pwm_set(pwm_t *pwm, uint16_t val);
void pwm_set_dutycycle(pwm_t *pwm, double val);
 int pwm_init(pwm_t *pwm, TC0_t *timer, TC_CLKSEL_t prescalar, uint8_t pwm_channel, 
		uint16_t base_frequency, uint16_t desiredFrequency);



#endif /* PWM_H_ */