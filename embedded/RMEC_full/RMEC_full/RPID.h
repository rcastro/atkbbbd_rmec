/*
 * RPID.h
 *
 * Keep in mind, min and max IN are only if it is CONT
 * Created: 10/7/2018 1:22:31 PM
 *  Author: Ruben Castro
 */ 


#ifndef RPID_H_
#define RPID_H_

#include "l_filter.h"

typedef struct{
	double P;
	double I;
	double D;
	double dt;
	unsigned int CONT:1;
	int MAX_IN;
	int MIN_IN;
	int MAX_OUT;
	int MIN_OUT;

	double prevError;
	double currentError;
	double totalError ;
	
	double setpoint;
	double acceptableRange;
	int onTargetTicks;
	
	l_filter_t filter;

}RPID_t;

void RPID_init(RPID_t *pid, double P, double I, double D, double dt,
		 uint8_t CONT, int MIN_IN, int MAX_IN, int MIN_OUT, int MAX_OUT, uint8_t K);
double RPID_crunch(RPID_t *pid, double input);
void RPID_setSetpoint(RPID_t *pid, double val);
void RPID_setAcceptableRange(RPID_t *pid, double range);
uint8_t RPID_onTarget(RPID_t *pid);


#endif /* RPID_H_ */