/*
* ATKMotor.c
*
* Created: 10/8/2018 2:32:35 PM
*  Author: Ruben Castro
*/

#include "motor.h"



void motor_init(motor_t *motor, pin_t *INA_pin, pin_t *INB_pin, pwm_t *PWM) {
	motor->INA_pin = INA_pin;
	motor->INB_pin = INB_pin;
	motor->PWM = PWM;
	

	pin_set(motor->INA_pin);
	pin_clear(motor->INB_pin);
}

void motor_set(motor_t *motor, double val){
	if(val<0){
		val*=-1;
		pin_set(motor->INA_pin);
		pin_clear(motor->INB_pin);
		}else{
		pin_set(motor->INB_pin);
		pin_clear(motor->INA_pin);
	}
	pwm_set_dutycycle(motor->PWM, val);
}

void motor_stop(motor_t *motor){
	motor_set(motor, 0);
}






