/*
 * current_controller.c
 *
 * Created: 10/16/2018 7:19:37 PM
 *  Author: Ruben Castro
 */ 
 #include "avr/io.h"
 #include "current_controller.h"
 #include "l_filter.h"
 #include "ATKBBBIO/RBasic.h"
 #include "ATKBBBIO/ATKDAC.h"
 #include <stdlib.h>


 double currentc_crunch(currentc_t *cc, double input){
	 double pValue, iValue;
	 cc->currentError = cc->setpoint - input;


	 cc->totalError+=cc->currentError* cc->dt;
	 cc->totalError = CONSTRAIN(cc->totalError, cc->minTotalError , cc->maxtotalError);

	 //pValue = Math.abs(currentError) < acceptableRange ? 0: p * currentError;
	 pValue = cc->P * cc->currentError;
	 iValue = cc->I * cc->totalError;
	 

	 cc->prevError = cc->currentError;
	 //return pValue + iValue + dValue;
	 
	 //check if on target
	 if(abs(cc->currentError) < cc->acceptableRange){
		 cc->onTargetTicks++;
		 }else{
		 cc->onTargetTicks=0;
	 }


	 return CONSTRAIN(pValue + iValue ,cc->MIN_OUT, cc->MAX_OUT);
 }


 void currentc_setSetpoint(currentc_t *cc, double val){
	 cc->setpoint=val;
 }

 void currentc_setAcceptableRange(currentc_t *cc, double range){
	 cc->acceptableRange=range;
 }

 uint8_t currentc_onTarget(currentc_t *cc){ // if on target for more than 50 millis

	 return cc->onTargetTicks * cc->dt >0.1;
 }


 void currentc_init(currentc_t *cc, double P, double I, double dt, int MIN_IN, int MAX_IN, int MIN_OUT, int MAX_OUT){
	 cc->P = P;
	 cc->I=I;
	 cc->dt=dt;
	 cc->MIN_IN=MIN_IN;
	 cc->MAX_IN=MAX_IN;
	 cc->MIN_OUT=MIN_OUT;
	 cc->MAX_OUT=MAX_OUT;
	 cc->minTotalError = MIN_OUT/ I;
	 cc->maxtotalError = MAX_OUT/ I;
	 
	 cc->prevError=0;
	 cc->currentError=0;
	 cc->totalError=0;
	 cc->setpoint=0;
	 cc->acceptableRange=20;

	 cc->onTargetTicks=0;
 }