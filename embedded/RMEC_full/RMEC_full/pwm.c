/*
* ATKPWM.c
*
* Created: 10/7/2018 3:04:22 PM
*  Author: Ruben Castro
*/

#include "avr/io.h"
#include "RBasic.h"
#include "pwm.h"
#include <math.h>


void pwm_set_dutycycle(pwm_t *pwm, double val){ //setPWM to value between 0 and 100
	uint16_t pwm_val = (CONSTRAIN(val,0.0,100.0)/100.0) * pwm->per;
	pwm_set(pwm,pwm_val);
}

void pwm_set(pwm_t *pwm, uint16_t val){
	uint8_t vall = (uint8_t) val;
	uint8_t valh = (uint8_t) (val >> 8);
	uint8_t channel = pwm->pwm_channel;

	if(channel == TC0_CCAEN_bm){
	//pwm->timer->CCABUFH = valh;
	//pwm->timer->CCABUFL = vall;
	pwm->timer->CCABUF=val;
	}else if(channel == TC0_CCBEN_bm){
	pwm->timer->CCBBUFH = valh;
	pwm->timer->CCBBUFL = vall;
	}else if(channel ==TC0_CCCEN_bm){
	pwm->timer->CCCBUFH = valh;
	pwm->timer->CCCBUFL = vall;
	}else if(channel == TC0_CCDEN_bm){
	pwm->timer->CCDBUFH = valh;
	pwm->timer->CCDBUFL = vall;
	}
}


//frequencies are limited due to prescalar and 16 bit numbers
int pwm_init(pwm_t *pwm, TC0_t *timer, TC_CLKSEL_t prescalar, uint8_t pwm_channel,
uint16_t base_frequency, uint16_t  desiredFrequency){//frequency in khz

	pwm->timer = timer;
	pwm->prescalar=prescalar;
	pwm->pwm_channel=pwm_channel;
	pwm->base_frequency = base_frequency;
	pwm->desired_frequency = desiredFrequency;

	timer->CTRLA = prescalar;

	timer->CTRLB |= TC_WGMODE_SINGLESLOPE_gc | pwm_channel;//wave generation mode single slope, enabling CCA open port OC
	int prescalardiv= pow(2,prescalar-1);//gets division from prescalar - taking advantage of way TC_CLKSEL_t is written
	prescalardiv=1;
	pwm->per = (base_frequency/prescalardiv)/(desiredFrequency); //  x = (BF/p)/DF Base Frequency, prescalar, desired frequency
	if(pwm->per >65535 || pwm->per<0){
		return -1;
	}

	uint8_t perl = (uint8_t) pwm->per;
	uint8_t perh = (uint8_t) (pwm->per >> 8);
	
	timer->PERBUFL = perl;//for the waveform period - controls TOP value
	timer->PERBUFH = perh;

	pwm_set(pwm,0);
	return 0;
}

