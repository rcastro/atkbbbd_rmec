/*
 * RMEC_Basic.c
 * Set the RMEC to output 12v on the motor.
 *
 * Created: 11/28/2018 11:36:22 PM
 * Author : Ruben Castro
 */ 

#include <avr/io.h>



int main(void)
{
	//outputs
    PORTC.DIRSET = PIN0_bm | PIN1_bm | PIN3_bm;
	
	
	while(1){
		//enable and set to forwards
		PORTC.OUTSET = PIN1_bm | PIN3_bm;
		PORTC.OUTCLR = PIN0_bm;
		
	}
}


