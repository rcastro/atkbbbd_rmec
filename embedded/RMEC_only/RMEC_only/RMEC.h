/*
 * RMEC.h
 *
 * Created: 11/29/2018 6:05:32 PM
 *  Author: Ruben Castro
 */ 


#ifndef RMEC_H_
#define RMEC_H_

 int RMEC_init(int baseFrequency);
 void RMEC_set(double val);
 int RMEC_current();
 void RMEC_brake();


#endif /* RMEC_H_ */