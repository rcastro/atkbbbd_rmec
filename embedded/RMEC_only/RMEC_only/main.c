/*
* RMEC_only.c
*
* Created: 11/29/2018 6:04:50 PM
* Author : Ruben Castro
*/

#include <avr/interrupt.h>
#include <avr/io.h>
#include "RMEC.h"

void clock_init(void){
	OSC.XOSCCTRL = OSC_XOSCSEL_XTAL_256CLK_gc | OSC_FRQRANGE_12TO16_gc; // select external source
	OSC.CTRL = OSC_XOSCEN_bm; // enable external source
	while(!(OSC.STATUS & OSC_XOSCRDY_bm)); // wait for external
	OSC.PLLCTRL = OSC_PLLSRC_XOSC_gc | OSC_PLLFAC0_bm | OSC_PLLFAC1_bm; // select external osc for pll, do pll = source * 3
	//OSC.PLLCTRL = OSC_PLLSRC_XOSC_gc | OSC_PLLFAC1_bm; // pll = source * 2 for 32MHz std clock
	OSC.CTRL |= OSC_PLLEN_bm; // enable PLL
	while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL for main clock
}

void timing_interrupts_init(){
	TCC1.CTRLA = TC_CLKSEL_DIV64_gc;

	uint16_t pera = 24000; //  31.25hz (48MHZ/24000/64)

	TCC1.PERBUF = pera;

	TCC1.INTCTRLA = TC_OVFINTLVL_MED_gc;
}


int main(void)
{
	clock_init();

	//start interrupts;
	//initialize interrupts
	sei();
	PMIC.CTRL |= PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
	timing_interrupts_init();

	/* Replace with your application code */
	RMEC_init(48000);
	while (1)
	{

	}
}

int i =0;
int b =0;
//2khz
ISR(TCC1_OVF_vect){
	i++;
	i++;
	if(!(i%16)){ //blink about once per second toggle twice per second
		PORTE_DIRTGL = PIN2_bm;
	}
	
	if(b==0){
		RMEC_set(i%200-100);
	}else{
		RMEC_set(100-i%200);
	}
	if(b>=100){
		b=1;
	}else if(b<=-100){
		b=0;
	}
	
}