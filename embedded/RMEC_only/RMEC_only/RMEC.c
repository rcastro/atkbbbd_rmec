/*
 * RMEC.c
 *
 * Created: 11/29/2018 6:05:20 PM
 *  Author: Ruben Castro
 */ 
 #define desiredFrequency 20
 #define mVpermA 150

 #include "RMEC.h"
 #include <avr/io.h>
 #include <stdlib.h>

 uint16_t per;


 //set the motor from a value between -100% and 100%
 void RMEC_set(double val){

	double valabs = abs(val);
	//map -100 - 100 to 0-per
	uint16_t pwm_val =  ((valabs/100.0) * per);

	if(val<0){
		TCC0.CCABUF = pwm_val;
		TCC0.CCBBUF = 0;
	}else{
		TCC0.CCABUF = 0;
		TCC0.CCBBUF = pwm_val;
	}
}

 int RMEC_current(){
	 return (((ADCB.CH0.RESH<<8) + ADCB.CH0.RESL -2018)*1.35 / mVpermA); //answer in mA
 }


  //brake mode is enabled when both inputs are 1, so set duty cycle to maximum (1)
void RMEC_brake(){
	  TCC0.CCBBUF=per;
	  TCC0.CCABUF=per;
}


int RMEC_init(int baseFrequency){
	
	//setup timer 0
	TCC0.CTRLA = TC_CLKSEL_DIV1_gc;
	TCC0.CTRLB = TC_WGMODE_SINGLESLOPE_gc | TC0_CCAEN_bm | TC0_CCBEN_bm;
	per = baseFrequency/desiredFrequency;
	if(per>65535 || per<0){
		return -1;
	}
	uint8_t perl = (uint8_t) per;
	uint8_t perh = (uint8_t) (per >> 8);
	
	TCC0.PERBUFL = perl;//for the waveform period - controls TOP value
	TCC0.PERBUFH = perh;

	//PWMs init at 20khz on Timer 0 - PC0 and PC1
	PORTC.DIRSET |= PIN0_bm | PIN1_bm;
	RMEC_brake();


	//ADC init

	ADCB.CTRLA |= 1<<0;
	ADCB.CTRLB |= ADC_RESOLUTION_12BIT_gc | ADC_FREERUN_bm;
	ADCB.REFCTRL  = ADC_REFSEL_INTVCC_gc;
	ADCB.PRESCALER = ADC_PRESCALER_DIV512_gc;

	PORTB.DIRCLR |= PIN0_bm;
	ADCB.CH0.CTRL = ADC_CH_INPUTMODE_DIFFWGAIN_gc | ADC_CH_GAIN_DIV2_gc;
	ADCB.CH0.MUXCTRL =  ADC_CH_MUXNEG_GND_MODE4_gc | ADC_CH_MUXPOS_PIN0_gc;


	//enable
	PORTC.DIRSET |= PIN3_bm;
	PORTC.OUTSET |= PIN3_bm;
	return 0;
}
