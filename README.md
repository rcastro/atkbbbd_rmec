This is my first attempt at a circuit board. It shall hold an H-bridge for 
motor control, current sensing, and SPI pins for encoder implementation.
It is a board that revolves around the DRV8701 (http://www.ti.com/product/DRV8701) 
gate driver chip. The DRV8701 is capable of driving a varied set of MOSFETS.
Chosen here are the AON7410 MOSFETS.

If you have one of these boards, check out the embedded folder and the RMEConly project for full integration with minimal overhead.
RMEC full offers some other IO classes I used initially to make initial testing easier.
RMEC basic just turns the motor in one direction at full speed.

Ports used
______________




## Design Rules:
-5A continuous/10A peak rating
-0-10A to 0-1.5V mapping current sensing
-12V Motor
-SPI breakout for encoder
-Not hotter than 70C


## DRV8701 Rules:
Vgs(voltage applied to MOSFET gate) = 9.5V
Ivcp(current capable of being applied to MOSFET gate - dependent on Vm) = 12mA  
20v/v conversion for current sense - for 0-10A to 0-1.5V mapping, you need a ~ 7 mOhm resistor rated for >=2watts
Lots of passive components need to be around the IC.  


Here is board version 2.1, updated for a new 'mother' board ... 

## Circuit

![routedTop](images/routed.png)



## Schematic

![routedBottom](images/schematic.png)




A few concerns here arise with v2.0 that need to be addressed by checking some
parameters in person, which I shall do tomorrow. 

# Problems

The fill for GND that goes towards the Motor GND has a choke point at the corner
due to the headers for uploading code being right next to the power and GND sources.
To solve this, I will move the MOSFETS towards the left and up, enough to get 
a decent amount of fill, but before then, I want to check how the physical
mounting of the programming female headers fit with the male headers on the board
to solidify the perimeter of the board.

The highest value that I have been able to get the ADC on the ATKBBB to read
is 1.5V. I need to test it again with every possible combination detailed in the
manual to see what the actual max is to consider changing the size of the 
shunt resistor. Right now it is set to 7mOhm, which would return to the ADC
150mV/A


# Solved Problems/Interesting Challenges
Due to the way the PWR and GND are conected, I had to get creative with the GND
planes. With the limited knowledge I have gathered. I limited the motor GND to the 
TOP plane, and kept the Bottom ground plane as clear as possible for the Gate Driver
, which required some creative routing of some components.







## Basic definitions I learned in the process-

Bulk Capacitor - Capacitor used to be capable of taking over battery in certain sags. It is
    set as close to the input as possible. In this case an electrolytic >=10uF.
    
Decoupling Capacitor- Capacitor used to give power to certain components or devices
    similar function to bulk capacitor, except placed near individual components.
    Like a local bulk capacitor.
    
Rsense -Shunt Resistor used for current sensing on the low side of the h-bridge.
    In this case- 7mOhm. Current can be measured by taking the difference between
    each side of the resistor to measure voltage drop, then V=IR
    
Net Ties- Just like a trace, except its meant to be isolated from other traces or
    pours around it. Used here for Rsense, where we want to measure difference
    between ground and the voltage before the resistor, but we want the traces
    to look about the same and not be impacted by differences in voltage around it.

Kelvin Connection- Way of connecting traces to Rsense. It just makes sense
    Take it for granted. Google how to do it. It's very simple.

many many more to be copied from my notebook soon.


# PCB guides:

45 degree traces only
Kelvin Connection for Rsense
Always have a ground plane.
Try to be as symmetric as possible
Differential pairs routing for pairs of traces.

Many many more to be copied from my notebook soon.

# V1.0 BOARD ARE IN!
It works!!!!!!
comments:
Would be great if it was separable from the ATKBBB
Would be great if there was another set of identical signal pins for probing
Would be better if PWM traces were separated more to prevent signal coupling
Capacitors for VM and VPH stuck together on both board on one end - separate a little more 

## Learning how to put components on the PCB.
Here's how I put together the components based on what Jake told me:
1. Lay out all your components first. It is really nice if you have a tray in 
which you can put your most used components onto so that you can just pick them 
out directly instead of going all the way to the reel or a bag.

![sicksetup](media/checkoutmysicksetup.jpg)

2. Pull out your stencil. It makes it really easy to put the solder paste on there.
Clean a surface and put your board(in my case a 2x2 array of the board which come together
to make multiple at a time) right on top. Line up the holes of the stencil with the
soldering pads on the board. I found that using any large pads on opposite ends of the board
were really useful for most of the aligning. Afterwards, grab a sizeable amount of solder paste
and put it onto the stencil. If it was refrigerated, wait for it to heat up a little, as
you want the paste to be easily moldable so it goes into holes easier. Then simply 
squeege the paste onto the holes from one side to the other while holding
down the stencil so it doesn't move. Push down hard as you squeege
so all the paste goes in. Take off your stencil and checkout your work under a microscope
to see if the paste aligns with the pads well enough!


![stencil](media/stencil.jpg)
![pastedboards](media/freshboardswithpaste.jpg)

3. Grab some tweezers with your thumb on one side and your index finger on the other.
Under the microscope place each component carefully. As I was placing them down
for the first time, I ended up bumping into other components and having to fix those too,
which took forever in the end. I also found it to be better not to push down when 
placing components, as it just smears the paste everywhere. Don't worry too much
about paste being in the wrong places though, when it is in the oven, it balls up
and collects itself around where it needs to be. Careful though, if components are
placed close, they can be pulled together. I made two boards, and in both boards
the only problem was that two capacitors got stuck together on one end in the exact
same place. I made a note to change it on Eagle for the next version, and I had to 
unsolder, and solder it back manually (which I found to be really hard!).

![solderingwoops](media/solderingwoops.jpg)
Here it is fixed up!
![solderingfixed](media/solderingfixed!.jpg)

Some more picks of the board under microscrope.
![mosfetsundermicroscope](media/mosfetsundermicroscope.jpg)
![gateundermicroscope](media/Gatedriverundermicroscope.jpg)

4. Finally! put it in the oven! For CBA oven, there is a switch at the right
end all the way to the back that turns it on, and then you click on the top option
when the boards are inside of the oven, which automatically started the whole
process. Then just wait until it says cooling down wait 10...9...8.. then just turn it off
and open up the door to cool it down. You can also leave it in the oven for a bit. 
Depends on how quickly you want to test them ;) 
![intheoven](media/intheoven.jpg)

5. Test out your parts! Here is a pic of this finished board!
![board](media/workingboard.jpg)


# Sources:

MOSFET: https://www.digikey.com/product-detail/en/alpha-omega-semiconductor-inc/AON7410/785-1581-1-ND/3621507

Gate Driver: http://www.ti.com/product/DRV8701/technicaldocuments <<< A bunch of great resources there for general pcb layout knowledge.

General TI-DRV-style Motor Driver PCB Layout manual: http://www.ti.com/general/docs/lit/getliterature.tsp?baseLiteratureNumber=slva959&fileType=pdf

Great Resource on ground planes - https://www.maximintegrated.com/en/app-notes/index.mvp/id/5450


# Jake's Notes  

I modified *some* aspects of the board, and pinouts go through the [xmega motherboard](https://gitlab.cba.mit.edu/jakeread/motherboard-xmega). 

Here are the pinouts as they stand:

**PB1:** The DRV8701's Sense Output (amplifier output)  
**PB6:** DRV8701 NFault  
**PB7:** DRV8701 SNSOut (logic / flag)  
  
**PC4:** DRV8701 NSleep  
**PC6:** IN1  
**PC7:** IN2  
  
**PD4:** PWM -> RC Filter -> DRV8701 VRef  
  
**PE4:** Encoder (SPI) Chip Select  
**PE5:** Encoder (SPI) Clock  
**PE6:** Encoder (SPI) MISO  
**PE7:** Encoder (SPI) MOSI  

### RC Filter to VREF, and Chopping

There is an RC Filter in front of the DRV8701's VRef. I assumed we would run 100kHz pwm into this RC filter, to output analog voltages that the DRV8701 will use in it's comparator / chopper drive. 

The R is 1kOhm and the C is 1uF, which *should* have a low-pass around 150kHz, but please check that I calculated that properly. 

The datasheet includes a note on which VRef voltages will lead to which chopping currents based on the size of the sense resistor. I placed a 50mOhm resistor on the board, but this can change. It was the recommended value in the datasheet.

### The IDrive Pin

I have a 10kOhm resistor here, but I think it would 'properly' be 33kOhm. I think this will *increase* the drive current, which will just waste a little bit of energy, no worries.

### Sense Out

The DRV8701's Sense Amplifier output is still connected to PB1 to be read on the ADC. 

Note:
Could use DAC to output a voltage to current chopper on the gate driver as opposed to running a pwm through an RC filter.