<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="12" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="2X6_SMD">
<description>&lt;h3&gt;Surface Mount - 2x6&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_06x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="-3.81" y="1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-6.35" y="1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-6.35" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-3.81" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-1.27" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-1.27" y="1.27" radius="0.7" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-2.5" x2="-7.62" y2="2.5" width="0.127" layer="51"/>
<wire x1="-7.62" y1="2.5" x2="7.62" y2="2.5" width="0.127" layer="51"/>
<wire x1="7.62" y1="2.5" x2="7.62" y2="-2.5" width="0.127" layer="51"/>
<wire x1="7.62" y1="-2.5" x2="-7.62" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-4.11" y1="2.55" x2="-3.51" y2="3.35" layer="51"/>
<rectangle x1="-6.65" y1="2.55" x2="-6.05" y2="3.35" layer="51"/>
<rectangle x1="-1.57" y1="2.55" x2="-0.97" y2="3.35" layer="51"/>
<rectangle x1="-6.65" y1="-3.35" x2="-6.05" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="-4.11" y1="-3.35" x2="-3.51" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="-1.57" y1="-3.35" x2="-0.97" y2="-2.55" layer="51" rot="R180"/>
<smd name="1" x="-6.35" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="2" x="-6.35" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="3" x="-3.81" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="4" x="-3.81" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="5" x="-1.27" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="6" x="-1.27" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<circle x="1.27" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="1.27" y="1.27" radius="0.7" width="0.127" layer="51"/>
<rectangle x1="0.97" y1="2.55" x2="1.57" y2="3.35" layer="51"/>
<rectangle x1="0.97" y1="-3.35" x2="1.57" y2="-2.55" layer="51" rot="R180"/>
<smd name="7" x="1.27" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="8" x="1.27" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<circle x="3.81" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="3.81" y="1.27" radius="0.7" width="0.127" layer="51"/>
<rectangle x1="3.51" y1="2.55" x2="4.11" y2="3.35" layer="51"/>
<rectangle x1="3.51" y1="-3.35" x2="4.11" y2="-2.55" layer="51" rot="R180"/>
<smd name="9" x="3.81" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="10" x="3.81" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<circle x="6.35" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="6.35" y="1.27" radius="0.7" width="0.127" layer="51"/>
<rectangle x1="6.05" y1="2.55" x2="6.65" y2="3.35" layer="51"/>
<rectangle x1="6.05" y1="-3.35" x2="6.65" y2="-2.55" layer="51" rot="R180"/>
<smd name="11" x="6.35" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="12" x="6.35" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<text x="-7.112" y="4.191" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-6.858" y="-4.699" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-7.239" y1="-3.81" x2="-7.239" y2="-2.794" width="0.2032" layer="21"/>
</package>
<package name="2X6">
<description>&lt;h3&gt;Plated Through Hole - 2x6&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_06x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.2032" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.2032" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.175" x2="9.525" y2="3.81" width="0.2032" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.795" y2="3.81" width="0.2032" layer="21"/>
<wire x1="10.795" y1="3.81" x2="11.43" y2="3.175" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.175" x2="12.065" y2="3.81" width="0.2032" layer="21"/>
<wire x1="12.065" y1="3.81" x2="13.335" y2="3.81" width="0.2032" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.97" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.175" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="3.175" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="11" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<pad name="12" x="12.7" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51"/>
<rectangle x1="12.446" y1="2.286" x2="12.954" y2="2.794" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.651" x2="-0.635" y2="-1.651" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<text x="-1.27" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X6_NOSILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="11" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<pad name="12" x="12.7" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51"/>
<rectangle x1="12.446" y1="2.286" x2="12.954" y2="2.794" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<text x="-1.27" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X01_LONGPAD">
<description>&lt;h3&gt;Plated Through Hole - Long Pad&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.27" y1="0.635" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01_2MM">
<description>&lt;h3&gt;Plated Through Hole - 2mm&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="2" diameter="3.302" rot="R90"/>
<text x="-1.651" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X01_OFFSET">
<description>&lt;h3&gt;Plated Through Hole - Long Pad w/ Offset Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="offset" rot="R90"/>
<text x="-1.27" y="3.048" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01_POGOPIN_HOLE_0.061_DIA">
<description>&lt;h3&gt;Pogo Pin - 0.061"&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.061"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90" thermals="no"/>
<hole x="0" y="0" drill="1.5494"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X01_POGOPIN_HOLE_0.58_DIA">
<description>&lt;h3&gt;Pogo Pin Hole - 0.58" &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.58"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90" thermals="no"/>
<hole x="0" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SNAP-FEMALE">
<description>&lt;h3&gt;Sew-On Fabric Snap - Female&lt;/h3&gt;
Equivalent to size #1/0 snap. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 1&lt;/li&gt;
&lt;li&gt;Area:8mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="2.921" diameter="4.572"/>
<polygon width="0.254" layer="1">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="29">
<vertex x="-4.0005" y="0" curve="-90.002865"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="31">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="41">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<text x="-1.27" y="4.318" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-4.953" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SNAP-MALE">
<description>&lt;h3&gt;Sew-On Fabric Snap - Male&lt;/h3&gt;
Equivalent to size #1/0 snap. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 1&lt;/li&gt;
&lt;li&gt;Area:8mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<smd name="2" x="0" y="0" dx="7.62" dy="7.62" layer="1" roundness="100"/>
<text x="-1.524" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-4.826" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SPRING-CONNECTOR">
<description>&lt;h3&gt;Spring Connector&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 1&lt;/li&gt;
&lt;li&gt;Area:0.25"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<smd name="P$2" x="0" y="0" dx="7.112" dy="7.112" layer="1" roundness="100"/>
<text x="-1.27" y="3.81" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-4.572" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X01NS_KIT">
<description>&lt;h3&gt;Plated Through Hole - No Silk Outline Kit Version&lt;/h3&gt;
&lt;p&gt; Mask on only one side to make soldering in kits easier.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<circle x="0" y="0" radius="0.508" width="0" layer="29"/>
<circle x="0" y="0" radius="0.9398" width="0" layer="30"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="1X01_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="SMTSO-256-ET-0.165DIA">
<description>&lt;h3&gt;SMTSO-256-ET Flush Mount Nut&lt;/h3&gt;
.165 drill
&lt;br&gt;
Fits 4-40 Screws. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 1&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="1.016" width="0.127" layer="51"/>
<wire x1="-1.016" y1="2.286" x2="-2.286" y2="1.016" width="1.016" layer="31" curve="42.075022"/>
<wire x1="2.286" y1="1.016" x2="1.016" y2="2.286" width="1.016" layer="31" curve="42.075022"/>
<wire x1="1.016" y1="-2.286" x2="2.286" y2="-1.016" width="1.016" layer="31" curve="42.075022"/>
<wire x1="-2.286" y1="-1.016" x2="-1.016" y2="-2.286" width="1.016" layer="31" curve="42.075022"/>
<pad name="P$1" x="0" y="0" drill="4.191" diameter="6.1976"/>
<text x="-1.397" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-3.937" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_06X2">
<description>&lt;h3&gt;12 Pin Connection&lt;/h3&gt;
6x2 pin layout</description>
<wire x1="6.35" y1="-2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="7.62" x2="5.08" y2="7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="5.08" x2="5.08" y2="5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="15.24" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="6.35" y2="15.24" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="15.24" x2="6.35" y2="15.24" width="0.4064" layer="94"/>
<wire x1="3.81" y1="12.7" x2="5.08" y2="12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="10.16" x2="5.08" y2="10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="12.7" x2="0" y2="12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="0" y2="10.16" width="0.6096" layer="94"/>
<text x="-1.27" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-1.27" y="15.748" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="10" x="10.16" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="10.16" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="10.16" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="10.16" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="10.16" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="-5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="-5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-5.08" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="1" x="-5.08" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="11" x="-5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="10.16" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
</symbol>
<symbol name="CONN_01">
<description>&lt;h3&gt;1 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="3.048" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="off" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_06X2" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_06X2" x="0" y="0"/>
</gates>
<devices>
<device name="SMD_FEMALE" package="2X6_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_FEMALE" package="2X6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-12423"/>
<attribute name="VALUE" value="2X6 FEMALE"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="2X6_NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_01" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Single connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;

&lt;p&gt;&lt;/p&gt;
On any of the 0.1 inch spaced packages, you can populate with these:
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;/p&gt;
This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt;
SMTSO-256-ET is a "flush mount" nut for a 4-40 screw. We mostly use this on specialty testbeds; it is a nice way to connect hardware to your PCB at an adjustable hieght.
&lt;p&gt;&lt;/p&gt;
Also note, the SNAP packages are for using a snappable style connector. We sell a baggie of snaps and they are also used on two LilyPad designs:
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11347"&gt; Snap Assortment - 30 pack (male and female)&lt;/a&gt; (DEV-11347)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10941"&gt;LilyPad Arduino SimpleSnap&lt;/a&gt; (DEV-10941)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10940"&gt; LilyPad SimpleSnap Protoboard&lt;/a&gt; (DEV-10940)&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_01" x="0" y="0"/>
</gates>
<devices>
<device name="PTH_LONGPAD" package="1X01_LONGPAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_2MM" package="1X01_2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="OFFSET" package="1X01_OFFSET">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLE_LARGE" package="1X01_POGOPIN_HOLE_0.061_DIA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLE_0.58" package="1X01_POGOPIN_HOLE_0.58_DIA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SNAP-FEMALE" package="SNAP-FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SNAP-MALE" package="SNAP-MALE">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-CONN" package="SPRING-CONNECTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-11822" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH_NO_SILK_KIT" package="1X01NS_KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_NO_SILK_YES_STOP" package="1X01_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMTSO-256-ET-0.165DIA" package="SMTSO-256-ET-0.165DIA">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="HW-08694" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
<package name="0603-CAP">
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="WSL2726">
<description>&lt;b&gt;SMD SHUNT RESISTOR&lt;/b&gt;</description>
<wire x1="-4.8768" y1="7.9756" x2="4.8768" y2="7.9756" width="0.2032" layer="51"/>
<wire x1="4.8768" y1="7.9756" x2="4.8768" y2="-0.3556" width="0.2032" layer="51"/>
<wire x1="4.8768" y1="-0.3556" x2="-4.8768" y2="-0.3556" width="0.2032" layer="51"/>
<wire x1="-4.8768" y1="-0.3556" x2="-4.8768" y2="7.9756" width="0.2032" layer="51"/>
<smd name="A" x="3.302" y="2.794" dx="2.4384" dy="5.588" layer="1"/>
<smd name="SA" x="3.302" y="6.9215" dx="2.4384" dy="0.889" layer="1"/>
<smd name="B" x="-3.302" y="2.794" dx="2.4384" dy="5.588" layer="1"/>
<smd name="SB" x="-3.302" y="6.9215" dx="2.4384" dy="0.889" layer="1"/>
<text x="-0.3556" y="-0.0508" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.5748" y="-0.0508" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="R2512_SHUNT">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="A" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1" thermals="no"/>
<smd name="B" x="2.8" y="0" dx="1.8" dy="3.2" layer="1" thermals="no"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<smd name="SA" x="-1.414" y="1.3" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="SB" x="1.386" y="1.3" dx="0.5" dy="0.5" layer="1"/>
<wire x1="-1.4" y1="1.3" x2="-2.4" y2="1.3" width="0.2032" layer="1"/>
<wire x1="1.4" y1="1.3" x2="2.2" y2="1.3" width="0.2032" layer="1"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="51"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="2220-C">
<smd name="P$1" x="-2.6" y="0" dx="1.2" dy="5" layer="1"/>
<smd name="P$2" x="2.6" y="0" dx="1.2" dy="5" layer="1"/>
<text x="-1.5" y="3" size="0.6096" layer="125">&gt;NAME</text>
<text x="-1.5" y="-3.5" size="0.6096" layer="127">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="R_SHUNT">
<wire x1="-3.81" y1="-0.889" x2="1.27" y2="-0.889" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.889" x2="-3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.889" x2="1.27" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.889" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.254" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.81" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-0.254" width="0.1524" layer="94"/>
<text x="-4.572" y="1.3716" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.826" y="-4.445" size="1.778" layer="96">&gt;VALUE</text>
<pin name="SB" x="-7.62" y="-2.54" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="SA" x="5.08" y="-2.54" visible="off" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="B" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="A" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2220" package="2220-C">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R_SHUNT" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="R_SHUNT" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="WSL2726">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="SA" pad="SA"/>
<connect gate="G$1" pin="SB" pad="SB"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512_SHUNT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="SA" pad="SA"/>
<connect gate="G$1" pin="SB" pad="SB"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="DSOP-ADVANCE">
<smd name="P$1" x="-1.905" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$2" x="-0.635" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$3" x="0.635" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$4" x="1.905" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$5" x="0" y="0.95" dx="5.5" dy="4.7" layer="1"/>
<circle x="-2.7" y="-3.8" radius="0.22360625" width="0.127" layer="21"/>
<wire x1="-2.5" y1="3" x2="2.5" y2="3" width="0.127" layer="48"/>
<wire x1="2.5" y1="3" x2="2.5" y2="-3" width="0.127" layer="48"/>
<wire x1="2.5" y1="-3" x2="-2.5" y2="-3" width="0.127" layer="48"/>
<wire x1="-2.5" y1="-3" x2="-2.5" y2="3" width="0.127" layer="48"/>
</package>
<package name="DRV8701">
<smd name="1" x="-1.9" y="1.25" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="-1.9" y="0.75" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="-1.9" y="0.25" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="-1.9" y="-0.25" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="-1.9" y="-0.75" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="-1.9" y="-1.25" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="18" x="1.9" y="1.25" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="17" x="1.9" y="0.75" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="16" x="1.9" y="0.25" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="15" x="1.9" y="-0.25" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="14" x="1.9" y="-0.75" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="13" x="1.9" y="-1.25" dx="0.24" dy="0.6" layer="1" rot="R90"/>
<smd name="24" x="-1.25" y="1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="23" x="-0.75" y="1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="22" x="-0.25" y="1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="21" x="0.25" y="1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="20" x="0.75" y="1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="19" x="1.25" y="1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="7" x="-1.25" y="-1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="8" x="-0.75" y="-1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="9" x="-0.25" y="-1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="10" x="0.25" y="-1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="11" x="0.75" y="-1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="12" x="1.25" y="-1.9" dx="0.24" dy="0.6" layer="1"/>
<smd name="25" x="0" y="0" dx="2.8" dy="2.8" layer="1"/>
<wire x1="-2" y1="2" x2="-2" y2="-2" width="0.127" layer="51"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.127" layer="51"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="51"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.127" layer="51"/>
<circle x="-2.15" y="1.2" radius="0.05" width="0.127" layer="21"/>
<wire x1="-2" y1="2" x2="-1.7" y2="2" width="0.127" layer="21"/>
<wire x1="-2.05" y1="2" x2="-2.05" y2="1.7" width="0.127" layer="21"/>
<wire x1="1.7" y1="2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="2" y2="1.7" width="0.127" layer="21"/>
<wire x1="2" y1="-1.7" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="1.7" y2="-2" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="-2" y2="-1.7" width="0.127" layer="21"/>
<text x="-1.85" y="2.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.05" y="-4.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="NFET-TPW4R008NH">
<description>MOSFET N-channel - Enhancement mode</description>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.254" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.254" width="0" layer="94"/>
<text x="-11.43" y="3.81" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
<text x="-11.43" y="1.27" size="1.778" layer="95" rot="MR180">&gt;NAME</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.508"/>
<vertex x="2.032" y="-0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<wire x1="3.302" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
</symbol>
<symbol name="DRV8701">
<pin name="VM" x="17.78" y="45.72" length="middle" rot="R180"/>
<pin name="VCP" x="-17.78" y="45.72" length="middle"/>
<pin name="CPH" x="-17.78" y="38.1" length="middle"/>
<pin name="CPL" x="-17.78" y="30.48" length="middle"/>
<pin name="GND" x="-17.78" y="-45.72" length="middle"/>
<pin name="VREF" x="-17.78" y="-20.32" length="middle"/>
<pin name="SH1" x="17.78" y="25.4" length="middle" rot="R180"/>
<pin name="GH1" x="17.78" y="35.56" length="middle" rot="R180"/>
<pin name="IN1" x="-17.78" y="5.08" length="middle"/>
<pin name="IN2" x="-17.78" y="0" length="middle"/>
<pin name="NSLEEP" x="-17.78" y="-5.08" length="middle"/>
<pin name="GH2" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="SH2" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="GL2" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="SP" x="17.78" y="-25.4" length="middle" rot="R180"/>
<pin name="SN" x="17.78" y="-38.1" length="middle" rot="R180"/>
<pin name="GL1" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="AVDD" x="-17.78" y="22.86" length="middle"/>
<pin name="DVDD" x="-17.78" y="15.24" length="middle"/>
<pin name="NFAULT" x="-17.78" y="-35.56" length="middle"/>
<pin name="SNSOUT" x="-17.78" y="-30.48" length="middle"/>
<pin name="SO" x="-17.78" y="-25.4" length="middle"/>
<pin name="IDRIVE" x="-17.78" y="-10.16" length="middle"/>
<text x="0" y="50.8" size="2.54" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-50.8" size="2.54" layer="96" align="center">&gt;VALUE</text>
<wire x1="-12.7" y1="-48.26" x2="-12.7" y2="48.26" width="0.254" layer="94"/>
<wire x1="-12.7" y1="48.26" x2="12.7" y2="48.26" width="0.254" layer="94"/>
<wire x1="12.7" y1="48.26" x2="12.7" y2="-48.26" width="0.254" layer="94"/>
<wire x1="12.7" y1="-48.26" x2="-12.7" y2="-48.26" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="NFET-TPW4R008NH" prefix="Q">
<gates>
<gate name="1" symbol="NFET-TPW4R008NH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DSOP-ADVANCE">
<connects>
<connect gate="1" pin="D" pad="P$5"/>
<connect gate="1" pin="G" pad="P$4"/>
<connect gate="1" pin="S" pad="P$1 P$2 P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DRV8701" prefix="U">
<description>This is the DRV8701, one of TI's gate drivers to make a full bridge for motor control. Includes 
current sense logic inside with an external sense resistor.</description>
<gates>
<gate name="G$1" symbol="DRV8701" x="0" y="0"/>
</gates>
<devices>
<device name="QFN24" package="DRV8701">
<connects>
<connect gate="G$1" pin="AVDD" pad="7"/>
<connect gate="G$1" pin="CPH" pad="3"/>
<connect gate="G$1" pin="CPL" pad="4"/>
<connect gate="G$1" pin="DVDD" pad="8"/>
<connect gate="G$1" pin="GH1" pad="17"/>
<connect gate="G$1" pin="GH2" pad="24"/>
<connect gate="G$1" pin="GL1" pad="19"/>
<connect gate="G$1" pin="GL2" pad="22"/>
<connect gate="G$1" pin="GND" pad="5 16 25"/>
<connect gate="G$1" pin="IDRIVE" pad="12"/>
<connect gate="G$1" pin="IN1" pad="15"/>
<connect gate="G$1" pin="IN2" pad="14"/>
<connect gate="G$1" pin="NFAULT" pad="9"/>
<connect gate="G$1" pin="NSLEEP" pad="13"/>
<connect gate="G$1" pin="SH1" pad="18"/>
<connect gate="G$1" pin="SH2" pad="23"/>
<connect gate="G$1" pin="SN" pad="20"/>
<connect gate="G$1" pin="SNSOUT" pad="10"/>
<connect gate="G$1" pin="SO" pad="11"/>
<connect gate="G$1" pin="SP" pad="21"/>
<connect gate="G$1" pin="VCP" pad="2"/>
<connect gate="G$1" pin="VM" pad="1"/>
<connect gate="G$1" pin="VREF" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="MINI-PCIE-ATK-MOTHER">
<smd name="15" x="-6.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="13" x="-7" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="11" x="-7.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="9" x="-8.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="7" x="-9.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="5" x="-10.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="3" x="-11" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="1" x="-11.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="17" x="-2.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="19" x="-1.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="21" x="-0.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="23" x="0.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="25" x="1" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="27" x="1.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="29" x="2.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="31" x="3.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="33" x="4.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="35" x="5" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="37" x="5.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="39" x="6.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="41" x="7.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="43" x="8.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="45" x="9" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="47" x="9.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="49" x="10.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="51" x="11.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="16" x="-5.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<wire x1="-4.6" y1="0" x2="-4.6" y2="3.25" width="0.127" layer="51"/>
<wire x1="-3.1" y1="3.25" x2="-3.1" y2="0" width="0.127" layer="51"/>
<smd name="14" x="-6.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="12" x="-7.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="10" x="-8.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="8" x="-9" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="6" x="-9.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="4" x="-10.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="2" x="-11.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="18" x="-1.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="20" x="-1" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="22" x="-0.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="24" x="0.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="26" x="1.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="28" x="2.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="30" x="3" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="32" x="3.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="34" x="4.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="36" x="5.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="38" x="6.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="40" x="7" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="42" x="7.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="44" x="8.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="46" x="9.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="48" x="10.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="50" x="11" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="52" x="11.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<wire x1="-3.1" y1="0" x2="12.85" y2="0" width="0.127" layer="51"/>
<wire x1="12.85" y1="0" x2="12.85" y2="3.25" width="0.127" layer="51"/>
<wire x1="15" y1="4" x2="13.6" y2="4" width="0.127" layer="51"/>
<wire x1="-13.6" y1="4" x2="-15" y2="4" width="0.127" layer="51"/>
<wire x1="-12.85" y1="3.25" x2="-13.6" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="12.85" y1="3.25" x2="13.6" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-3.1" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="-4.6" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-12.85" y1="3.25" x2="-12.85" y2="0" width="0.127" layer="51"/>
<wire x1="-12.85" y1="0" x2="-4.6" y2="0" width="0.127" layer="51"/>
<wire x1="-15" y1="-5.1" x2="15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="15" y1="-5.1" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="15" y1="-3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="15" y1="3.5" x2="15" y2="5.1" width="0.127" layer="51"/>
<wire x1="15" y1="5.1" x2="-15" y2="5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="5.1" x2="-15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="-15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="-15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15.8" y1="-2.5" x2="15.8" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-15.9" y1="-5.1" x2="-13.8" y2="-2.1" layer="51"/>
<rectangle x1="13.8" y1="-5.1" x2="15.9" y2="-2.1" layer="51"/>
<pad name="P$1" x="16.5" y="8.5" drill="4.5" diameter="6.5"/>
<pad name="P$2" x="-16.5" y="8.5" drill="4.5" diameter="6.5"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="11.6"/>
<vertex x="-16.8" y="11.1" curve="-73.739933"/>
<vertex x="-17" y="10.8" curve="60.752764"/>
<vertex x="-18.8" y="8.9" curve="-80.286401"/>
<vertex x="-19.2" y="8.7" curve="6.546325"/>
<vertex x="-19.6" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="5.4"/>
<vertex x="-16.2" y="5.9" curve="-73.739933"/>
<vertex x="-16" y="6.2" curve="60.752764"/>
<vertex x="-14.2" y="8" curve="-80.286401"/>
<vertex x="-13.8" y="8.3" curve="6.546325"/>
<vertex x="-13.4" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="8.2"/>
<vertex x="-19.1" y="8.2" curve="-73.739933"/>
<vertex x="-18.8" y="8" curve="60.752764"/>
<vertex x="-17" y="6.2" curve="-80.286401"/>
<vertex x="-16.7" y="5.8" curve="6.546325"/>
<vertex x="-16.7" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="8.8"/>
<vertex x="-13.9" y="8.8" curve="-73.739933"/>
<vertex x="-14.2" y="9" curve="60.752764"/>
<vertex x="-16.1" y="10.8" curve="-80.286401"/>
<vertex x="-16.3" y="11.1" curve="6.546325"/>
<vertex x="-16.3" y="11.6" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.2" y="11.6"/>
<vertex x="16.2" y="11.1" curve="-73.739933"/>
<vertex x="16" y="10.8" curve="60.752764"/>
<vertex x="14.2" y="8.9" curve="-80.286401"/>
<vertex x="13.8" y="8.7" curve="6.546325"/>
<vertex x="13.4" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.8" y="5.4"/>
<vertex x="16.8" y="5.9" curve="-73.739933"/>
<vertex x="17" y="6.2" curve="60.752764"/>
<vertex x="18.8" y="8" curve="-80.286401"/>
<vertex x="19.2" y="8.3" curve="6.546325"/>
<vertex x="19.6" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.4" y="8.2"/>
<vertex x="13.9" y="8.2" curve="-73.739933"/>
<vertex x="14.2" y="8" curve="60.752764"/>
<vertex x="16" y="6.2" curve="-80.286401"/>
<vertex x="16.3" y="5.8" curve="6.546325"/>
<vertex x="16.3" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.6" y="8.8"/>
<vertex x="19.1" y="8.8" curve="-73.739933"/>
<vertex x="18.8" y="9" curve="60.752764"/>
<vertex x="16.9" y="10.8" curve="-80.286401"/>
<vertex x="16.7" y="11.1" curve="6.546325"/>
<vertex x="16.7" y="11.6" curve="-90"/>
</polygon>
</package>
<package name="MINI-PCIE-ATK-DAUGHTER">
<smd name="15" x="-6.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="13" x="-7" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="11" x="-7.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="9" x="-8.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="7" x="-9.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="5" x="-10.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="3" x="-11" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="1" x="-11.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="17" x="-2.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="19" x="-1.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="21" x="-0.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="23" x="0.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="25" x="1" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="27" x="1.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="29" x="2.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="31" x="3.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="33" x="4.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="35" x="5" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="37" x="5.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="39" x="6.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="41" x="7.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="43" x="8.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="45" x="9" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="47" x="9.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="49" x="10.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="51" x="11.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="16" x="-5.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<wire x1="-4.6" y1="0" x2="-4.6" y2="3.25" width="0.127" layer="51"/>
<wire x1="-3.1" y1="3.25" x2="-3.1" y2="0" width="0.127" layer="51"/>
<smd name="14" x="-6.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="12" x="-7.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="10" x="-8.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="8" x="-9" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="6" x="-9.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="4" x="-10.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="2" x="-11.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="18" x="-1.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="20" x="-1" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="22" x="-0.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="24" x="0.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="26" x="1.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="28" x="2.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="30" x="3" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="32" x="3.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="34" x="4.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="36" x="5.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="38" x="6.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="40" x="7" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="42" x="7.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="44" x="8.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="46" x="9.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="48" x="10.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="50" x="11" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="52" x="11.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<wire x1="-3.1" y1="0" x2="12.85" y2="0" width="0.127" layer="51"/>
<wire x1="12.85" y1="0" x2="12.85" y2="3.25" width="0.127" layer="51"/>
<wire x1="-13.6" y1="4" x2="-19.5" y2="4" width="0.127" layer="51"/>
<wire x1="-12.85" y1="3.25" x2="-13.6" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="12.85" y1="3.25" x2="13.6" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-3.1" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="-4.6" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-12.85" y1="3.25" x2="-12.85" y2="0" width="0.127" layer="51"/>
<wire x1="-12.85" y1="0" x2="-4.6" y2="0" width="0.127" layer="51"/>
<wire x1="-15" y1="-5.1" x2="15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="15" y1="-5.1" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="15" y1="-3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="15" y1="3.5" x2="15" y2="5.1" width="0.127" layer="51"/>
<wire x1="15" y1="5.1" x2="-15" y2="5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="5.1" x2="-15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="-15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="-15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15.8" y1="-2.5" x2="15.8" y2="-2.5" width="0.127" layer="51"/>
<pad name="P$1" x="16.5" y="8.5" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$2" x="-16.5" y="8.5" drill="4.5" diameter="6.5" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="11.6"/>
<vertex x="-16.8" y="11.1" curve="-73.739933"/>
<vertex x="-17" y="10.8" curve="60.752764"/>
<vertex x="-18.8" y="8.9" curve="-80.286401"/>
<vertex x="-19.2" y="8.7" curve="6.546325"/>
<vertex x="-19.6" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="5.4"/>
<vertex x="-16.2" y="5.9" curve="-73.739933"/>
<vertex x="-16" y="6.2" curve="60.752764"/>
<vertex x="-14.2" y="8" curve="-80.286401"/>
<vertex x="-13.8" y="8.3" curve="6.546325"/>
<vertex x="-13.4" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="8.2"/>
<vertex x="-19.1" y="8.2" curve="-73.739933"/>
<vertex x="-18.8" y="8" curve="60.752764"/>
<vertex x="-17" y="6.2" curve="-80.286401"/>
<vertex x="-16.7" y="5.8" curve="6.546325"/>
<vertex x="-16.7" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="8.8"/>
<vertex x="-13.9" y="8.8" curve="-73.739933"/>
<vertex x="-14.2" y="9" curve="60.752764"/>
<vertex x="-16.1" y="10.8" curve="-80.286401"/>
<vertex x="-16.3" y="11.1" curve="6.546325"/>
<vertex x="-16.3" y="11.6" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.2" y="11.6"/>
<vertex x="16.2" y="11.1" curve="-73.739933"/>
<vertex x="16" y="10.8" curve="60.752764"/>
<vertex x="14.2" y="8.9" curve="-80.286401"/>
<vertex x="13.8" y="8.7" curve="6.546325"/>
<vertex x="13.4" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.8" y="5.4"/>
<vertex x="16.8" y="5.9" curve="-73.739933"/>
<vertex x="17" y="6.2" curve="60.752764"/>
<vertex x="18.8" y="8" curve="-80.286401"/>
<vertex x="19.2" y="8.3" curve="6.546325"/>
<vertex x="19.6" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.4" y="8.2"/>
<vertex x="13.9" y="8.2" curve="-73.739933"/>
<vertex x="14.2" y="8" curve="60.752764"/>
<vertex x="16" y="6.2" curve="-80.286401"/>
<vertex x="16.3" y="5.8" curve="6.546325"/>
<vertex x="16.3" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.6" y="8.8"/>
<vertex x="19.1" y="8.8" curve="-73.739933"/>
<vertex x="18.8" y="9" curve="60.752764"/>
<vertex x="16.9" y="10.8" curve="-80.286401"/>
<vertex x="16.7" y="11.1" curve="6.546325"/>
<vertex x="16.7" y="11.6" curve="-90"/>
</polygon>
<hole x="-12.5" y="0" drill="1.6"/>
<hole x="12.5" y="0" drill="1.1"/>
<smd name="P$3" x="14.65" y="-3.5" dx="2.3" dy="3.2" layer="1" rot="R180"/>
<smd name="P$4" x="-14.65" y="-3.5" dx="2.3" dy="3.2" layer="1" rot="R180"/>
<wire x1="-19.5" y1="4" x2="-20" y2="4.5" width="0.127" layer="51" curve="-90"/>
<wire x1="-20" y1="4.5" x2="-20" y2="34" width="0.127" layer="51"/>
<wire x1="-20" y1="34" x2="-19.5" y2="34.5" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="34.5" x2="19.5" y2="34.5" width="0.127" layer="51"/>
<wire x1="13.6" y1="4" x2="19.5" y2="4" width="0.127" layer="51"/>
<wire x1="19.5" y1="4" x2="20" y2="4.5" width="0.127" layer="51" curve="90"/>
<wire x1="20" y1="4.5" x2="20" y2="34" width="0.127" layer="51"/>
<wire x1="20" y1="34" x2="19.5" y2="34.5" width="0.127" layer="51" curve="90"/>
<wire x1="-19.5" y1="45" x2="19.5" y2="45" width="0.127" layer="51"/>
<wire x1="-20" y1="34" x2="-20" y2="44.5" width="0.127" layer="51"/>
<wire x1="-20" y1="44.5" x2="-19.5" y2="45" width="0.127" layer="51" curve="-90"/>
<wire x1="19.5" y1="45" x2="20" y2="44.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="44.5" x2="20" y2="34" width="0.127" layer="51"/>
</package>
<package name="ATK-BFPP-BOTTOM-FX8-60">
<smd name="P$1" x="-8.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<pad name="P$61" x="-16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$62" x="16.4" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<smd name="P$60" x="-8.6" y="2.85" dx="0.35" dy="1.7" layer="16"/>
<rectangle x1="-12.3" y1="-2.8" x2="12.3" y2="2.8" layer="52"/>
<hole x="-11.525" y="-1.8" drill="1.1"/>
<hole x="11.525" y="-1.8" drill="0.7"/>
<smd name="P$2" x="-8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$3" x="-7.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$4" x="-6.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$5" x="-6.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$6" x="-5.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$7" x="-5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$8" x="-4.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$9" x="-3.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$10" x="-3.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$11" x="-2.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$12" x="-2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$13" x="-1.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$14" x="-0.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$15" x="-0.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$16" x="0.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$17" x="1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$18" x="1.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$19" x="2.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$20" x="2.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$21" x="3.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$22" x="4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$23" x="4.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$24" x="5.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$25" x="5.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$26" x="6.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$27" x="7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$28" x="7.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$29" x="8.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$30" x="8.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$31" x="8.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$32" x="8.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$33" x="7.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$34" x="7" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$35" x="6.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$36" x="5.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$37" x="5.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$38" x="4.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$39" x="4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$40" x="3.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$41" x="2.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$42" x="2.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$43" x="1.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$44" x="1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$45" x="0.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$46" x="-0.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$47" x="-0.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$48" x="-1.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$49" x="-2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$50" x="-2.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$51" x="-3.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$52" x="-3.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$53" x="-4.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$54" x="-5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$55" x="-5.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$56" x="-6.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$57" x="-6.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$58" x="-7.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$59" x="-8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="3.1"/>
<vertex x="-16.8" y="2.3" curve="90"/>
<vertex x="-18.8" y="0.3"/>
<vertex x="-19.6" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="-0.3"/>
<vertex x="-18.8" y="-0.3" curve="90"/>
<vertex x="-16.8" y="-2.3"/>
<vertex x="-16.8" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="-3.1"/>
<vertex x="-16.2" y="-2.3" curve="90"/>
<vertex x="-14.2" y="-0.3"/>
<vertex x="-13.4" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="0.3"/>
<vertex x="-14.2" y="0.3" curve="90"/>
<vertex x="-16.2" y="2.3"/>
<vertex x="-16.2" y="3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.1" y="3.1"/>
<vertex x="16.1" y="2.3" curve="90"/>
<vertex x="14.1" y="0.3"/>
<vertex x="13.3" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.3" y="-0.3"/>
<vertex x="14.1" y="-0.3" curve="90"/>
<vertex x="16.1" y="-2.3"/>
<vertex x="16.1" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.7" y="-3.1"/>
<vertex x="16.7" y="-2.3" curve="90"/>
<vertex x="18.7" y="-0.3"/>
<vertex x="19.5" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.5" y="0.3"/>
<vertex x="18.7" y="0.3" curve="90"/>
<vertex x="16.7" y="2.3"/>
<vertex x="16.7" y="3.1" curve="-90"/>
</polygon>
</package>
<package name="ATK-BFPP-TOP-FX8-60">
<smd name="P$1" x="-8.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<pad name="P$61" x="-16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$62" x="16.4" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<smd name="P$60" x="-8.6" y="2.7" dx="0.35" dy="1.7" layer="1"/>
<rectangle x1="-12.3" y1="-2.55" x2="12.3" y2="2.55" layer="51"/>
<hole x="-11.525" y="-1.8" drill="1.1"/>
<hole x="11.525" y="-1.8" drill="0.7"/>
<smd name="P$2" x="-8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$3" x="-7.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$4" x="-6.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$5" x="-6.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$6" x="-5.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$7" x="-5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$8" x="-4.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$9" x="-3.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$10" x="-3.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$11" x="-2.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$12" x="-2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$13" x="-1.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$14" x="-0.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$15" x="-0.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$16" x="0.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$17" x="1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$18" x="1.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$19" x="2.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$20" x="2.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$21" x="3.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$22" x="4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$23" x="4.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$24" x="5.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$25" x="5.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$26" x="6.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$27" x="7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$28" x="7.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$29" x="8.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$30" x="8.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$31" x="8.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$32" x="8.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$33" x="7.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$34" x="7" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$35" x="6.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$36" x="5.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$37" x="5.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$38" x="4.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$39" x="4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$40" x="3.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$41" x="2.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$42" x="2.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$43" x="1.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$44" x="1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$45" x="0.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$46" x="-0.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$47" x="-0.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$48" x="-1.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$49" x="-2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$50" x="-2.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$51" x="-3.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$52" x="-3.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$53" x="-4.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$54" x="-5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$55" x="-5.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$56" x="-6.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$57" x="-6.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$58" x="-7.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$59" x="-8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="3.1"/>
<vertex x="-16.8" y="2.3" curve="90"/>
<vertex x="-18.8" y="0.3"/>
<vertex x="-19.6" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="-0.3"/>
<vertex x="-18.8" y="-0.3" curve="90"/>
<vertex x="-16.8" y="-2.3"/>
<vertex x="-16.8" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="-3.1"/>
<vertex x="-16.2" y="-2.3" curve="90"/>
<vertex x="-14.2" y="-0.3"/>
<vertex x="-13.4" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="0.3"/>
<vertex x="-14.2" y="0.3" curve="90"/>
<vertex x="-16.2" y="2.3"/>
<vertex x="-16.2" y="3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.1" y="3.1"/>
<vertex x="16.1" y="2.3" curve="90"/>
<vertex x="14.1" y="0.3"/>
<vertex x="13.3" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.3" y="-0.3"/>
<vertex x="14.1" y="-0.3" curve="90"/>
<vertex x="16.1" y="-2.3"/>
<vertex x="16.1" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.7" y="-3.1"/>
<vertex x="16.7" y="-2.3" curve="90"/>
<vertex x="18.7" y="-0.3"/>
<vertex x="19.5" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.5" y="0.3"/>
<vertex x="18.7" y="0.3" curve="90"/>
<vertex x="16.7" y="2.3"/>
<vertex x="16.7" y="3.1" curve="-90"/>
</polygon>
<wire x1="-20" y1="-3.5" x2="-19.5" y2="-4" width="0.127" layer="51" curve="90"/>
<wire x1="-19.5" y1="-4" x2="19.5" y2="-4" width="0.127" layer="51"/>
<wire x1="19.5" y1="-4" x2="20" y2="-3.5" width="0.127" layer="51" curve="90"/>
<wire x1="-20" y1="-3.5" x2="-20" y2="29.5" width="0.127" layer="51"/>
<wire x1="-20" y1="29.5" x2="-19.5" y2="30" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="30" x2="19.5" y2="30" width="0.127" layer="51"/>
<wire x1="19.5" y1="30" x2="20" y2="29.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="29.5" x2="20" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-20" y1="29.5" x2="-20" y2="44.5" width="0.127" layer="51"/>
<wire x1="-20" y1="44.5" x2="-19.5" y2="45" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="45" x2="19.5" y2="45" width="0.127" layer="51"/>
<wire x1="19.5" y1="45" x2="20" y2="44.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="44.5" x2="20" y2="29.5" width="0.127" layer="51"/>
<circle x="-20" y="5" radius="1.6" width="0.127" layer="51"/>
<circle x="-20" y="5" radius="2.75" width="0.127" layer="51"/>
<circle x="20" y="5" radius="1.6" width="0.127" layer="51"/>
<circle x="20" y="5" radius="2.75" width="0.127" layer="51"/>
</package>
<package name="JST-6-SMD-HORI-1.0MM">
<description>&lt;h3&gt;JST SH Vertical 6-Pin SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch: 1 mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/GPS/EM406-SMDConnector-eSH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;JST_6PIN_VERTICAL&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="-3.44" y="-5.16" radius="0.1047" width="0.4064" layer="21"/>
<wire x1="-2.9" y1="-0.1" x2="2.9" y2="-0.1" width="0.254" layer="21"/>
<wire x1="-4" y1="-2.1" x2="-4" y2="-4.4" width="0.254" layer="21"/>
<wire x1="3.1" y1="-4.4" x2="4" y2="-4.4" width="0.254" layer="21"/>
<wire x1="4" y1="-4.4" x2="4" y2="-2.1" width="0.254" layer="21"/>
<wire x1="-4" y1="-4.4" x2="-3.1" y2="-4.4" width="0.254" layer="21"/>
<smd name="1" x="-2.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-1.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="-0.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="0.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="2.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="M1" x="-3.8" y="-0.9" dx="1.2" dy="1.8" layer="1"/>
<smd name="M2" x="3.8" y="-0.9" dx="1.2" dy="1.8" layer="1"/>
<text x="-1.524" y="0.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.905" y="-6.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SJFAB">
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.7874" y="0" dx="1.1176" dy="1.6002" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="1.1176" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
</package>
<package name="ATK-BFPP-THRU-FX8-60">
<smd name="P$1" x="-8.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<pad name="P$61" x="-16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$62" x="16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<smd name="P$60" x="-8.6" y="2.7" dx="0.35" dy="1.7" layer="1"/>
<rectangle x1="-12.3" y1="-2.55" x2="12.3" y2="2.55" layer="51"/>
<hole x="-11.525" y="-1.8" drill="1.1"/>
<hole x="11.525" y="-1.8" drill="0.7"/>
<smd name="P$2" x="-8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$3" x="-7.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$4" x="-6.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$5" x="-6.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$6" x="-5.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$7" x="-5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$8" x="-4.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$9" x="-3.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$10" x="-3.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$11" x="-2.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$12" x="-2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$13" x="-1.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$14" x="-0.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$15" x="-0.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$16" x="0.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$17" x="1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$18" x="1.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$19" x="2.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$20" x="2.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$21" x="3.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$22" x="4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$23" x="4.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$24" x="5.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$25" x="5.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$26" x="6.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$27" x="7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$28" x="7.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$29" x="8.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$30" x="8.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$31" x="8.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$32" x="8.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$33" x="7.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$34" x="7" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$35" x="6.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$36" x="5.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$37" x="5.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$38" x="4.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$39" x="4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$40" x="3.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$41" x="2.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$42" x="2.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$43" x="1.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$44" x="1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$45" x="0.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$46" x="-0.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$47" x="-0.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$48" x="-1.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$49" x="-2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$50" x="-2.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$51" x="-3.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$52" x="-3.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$53" x="-4.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$54" x="-5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$55" x="-5.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$56" x="-6.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$57" x="-6.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$58" x="-7.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$59" x="-8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="3.1"/>
<vertex x="-16.8" y="2.3" curve="90"/>
<vertex x="-18.8" y="0.3"/>
<vertex x="-19.6" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="-0.3"/>
<vertex x="-18.8" y="-0.3" curve="90"/>
<vertex x="-16.8" y="-2.3"/>
<vertex x="-16.8" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="-3.1"/>
<vertex x="-16.2" y="-2.3" curve="90"/>
<vertex x="-14.2" y="-0.3"/>
<vertex x="-13.4" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="0.3"/>
<vertex x="-14.2" y="0.3" curve="90"/>
<vertex x="-16.2" y="2.3"/>
<vertex x="-16.2" y="3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.2" y="3.1"/>
<vertex x="16.2" y="2.3" curve="90"/>
<vertex x="14.2" y="0.3"/>
<vertex x="13.4" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.4" y="-0.3"/>
<vertex x="14.2" y="-0.3" curve="90"/>
<vertex x="16.2" y="-2.3"/>
<vertex x="16.2" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.8" y="-3.1"/>
<vertex x="16.8" y="-2.3" curve="90"/>
<vertex x="18.8" y="-0.3"/>
<vertex x="19.6" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.6" y="0.3"/>
<vertex x="18.8" y="0.3" curve="90"/>
<vertex x="16.8" y="2.3"/>
<vertex x="16.8" y="3.1" curve="-90"/>
</polygon>
<wire x1="-20" y1="-3.5" x2="-19.5" y2="-4" width="0.127" layer="51" curve="90"/>
<wire x1="-19.5" y1="-4" x2="19.5" y2="-4" width="0.127" layer="51"/>
<wire x1="19.5" y1="-4" x2="20" y2="-3.5" width="0.127" layer="51" curve="90"/>
<wire x1="-20" y1="-3.5" x2="-20" y2="29.5" width="0.127" layer="51"/>
<wire x1="-20" y1="29.5" x2="-19.5" y2="30" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="30" x2="19.5" y2="30" width="0.127" layer="51"/>
<wire x1="19.5" y1="30" x2="20" y2="29.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="29.5" x2="20" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-20" y1="29.5" x2="-20" y2="44.5" width="0.127" layer="51"/>
<wire x1="-20" y1="44.5" x2="-19.5" y2="45" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="45" x2="19.5" y2="45" width="0.127" layer="51"/>
<wire x1="19.5" y1="45" x2="20" y2="44.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="44.5" x2="20" y2="29.5" width="0.127" layer="51"/>
<smd name="P$63" x="-8.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$64" x="-8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$65" x="-7.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$66" x="-6.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$67" x="-6.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$68" x="-5.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$69" x="-5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$70" x="-4.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$71" x="-3.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$72" x="-3.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$73" x="-2.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$74" x="-2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$75" x="-1.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$76" x="-0.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$77" x="-0.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$78" x="0.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$79" x="1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$80" x="1.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$81" x="2.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$82" x="2.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$83" x="3.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$84" x="4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$85" x="4.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$86" x="5.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$87" x="5.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$88" x="6.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$89" x="7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$90" x="7.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$91" x="8.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$92" x="8.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$93" x="8.8" y="2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$94" x="8.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$95" x="7.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$96" x="7" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$97" x="6.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$98" x="5.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$99" x="5.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$100" x="4.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$101" x="4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$102" x="3.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$103" x="2.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$104" x="2.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$105" x="1.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$106" x="1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$107" x="0.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$108" x="-0.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$109" x="-0.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$110" x="-1.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$111" x="-2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$112" x="-2.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$113" x="-3.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$114" x="-3.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$115" x="-4.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$116" x="-5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$117" x="-5.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$118" x="-6.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$119" x="-6.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$120" x="-7.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$121" x="-8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$122" x="-8.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<pad name="P$123" x="-8.6" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$124" x="-8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$125" x="-7.4" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$126" x="-6.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$127" x="-6.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$128" x="-5.6" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$129" x="-5" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$130" x="-4.4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$131" x="-3.8" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$132" x="-3.2" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$133" x="-2.6" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$134" x="-2" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$135" x="-1.4" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$136" x="-0.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$137" x="-0.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$138" x="0.4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$139" x="1" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$140" x="1.6" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$141" x="2.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$142" x="2.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$143" x="3.4" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$144" x="4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$145" x="4.6" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$146" x="5.2" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$147" x="5.8" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$148" x="6.4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$149" x="7" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$150" x="7.6" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$151" x="8.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$152" x="8.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$153" x="8.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$154" x="8.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$155" x="7.6" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$156" x="7" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$157" x="6.4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$158" x="5.8" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$159" x="5.2" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$160" x="4.6" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$161" x="4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$162" x="3.4" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$163" x="2.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$164" x="2.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$165" x="1.6" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$166" x="1" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$167" x="0.4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$168" x="-0.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$169" x="-0.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$170" x="-1.4" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$171" x="-2" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$172" x="-2.6" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$173" x="-3.2" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$174" x="-3.8" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$175" x="-4.4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$176" x="-5" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$177" x="-5.6" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$178" x="-6.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$179" x="-6.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$180" x="-7.4" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$181" x="-8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$182" x="-8.6" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="ATK-BFPP">
<pin name="GND" x="-10.16" y="30.48" length="middle"/>
<pin name="+3V3" x="-10.16" y="15.24" length="middle"/>
<pin name="+5V" x="-10.16" y="7.62" length="middle"/>
<pin name="A0/AREF---PB0/ADC" x="-10.16" y="0" length="middle"/>
<pin name="A1/ADC---PB1/ADC" x="-10.16" y="-2.54" length="middle"/>
<pin name="A2/ADC/DAC---PB2/ADC/DAC" x="-10.16" y="-5.08" length="middle"/>
<pin name="A3/ADC/DAC---PB3/ADC/DAC" x="-10.16" y="-7.62" length="middle"/>
<pin name="A4/ADC---PB4/ADC" x="-10.16" y="-10.16" length="middle"/>
<pin name="A5/ADC---PB5/ADC" x="-10.16" y="-12.7" length="middle"/>
<pin name="A6/ADC---PB6/ADC/ADCOUT" x="-10.16" y="-15.24" length="middle"/>
<pin name="A7/ADC---PB7/ADC/ADCOUT" x="-10.16" y="-17.78" length="middle"/>
<pin name="C0/PWMALS---PC0/PWMALS/CSN/SDA" x="-10.16" y="-22.86" length="middle"/>
<pin name="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" x="-10.16" y="-25.4" length="middle"/>
<pin name="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" x="-10.16" y="-27.94" length="middle"/>
<pin name="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" x="-10.16" y="-30.48" length="middle"/>
<pin name="C4/PWMCLS---PC4/PWMCLS/CSN" x="-10.16" y="-33.02" length="middle"/>
<pin name="C5/PWMCHS---PC5/PWMCHS/SCK" x="-10.16" y="-35.56" length="middle"/>
<pin name="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" x="-10.16" y="-38.1" length="middle"/>
<pin name="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" x="-10.16" y="-40.64" length="middle"/>
<pin name="D0/USART/CSN---PD0/PWMA/CSN" x="-10.16" y="-45.72" length="middle"/>
<pin name="D1/USART/CLK---PD1/PWMB/SCK" x="-10.16" y="-48.26" length="middle"/>
<pin name="D2/USART/RX---PD2/PWMC/MISO/UARTRX" x="-10.16" y="-50.8" length="middle"/>
<pin name="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" x="-10.16" y="-53.34" length="middle"/>
<pin name="D4/PWMELS---PD4" x="-10.16" y="-58.42" length="middle"/>
<pin name="D5/PWMEHS---PD5" x="-10.16" y="-60.96" length="middle"/>
<pin name="E0/SDA---PE0" x="-10.16" y="-66.04" length="middle"/>
<pin name="E1/SCL---PE1" x="-10.16" y="-68.58" length="middle"/>
<pin name="E2/USARTRX---PE2" x="-10.16" y="-73.66" length="middle"/>
<pin name="E3/USARTTX---PE3" x="-10.16" y="-76.2" length="middle"/>
<pin name="E4/USART/CSN---PE4/PWMA/CSN" x="-10.16" y="-81.28" length="middle"/>
<pin name="E5/USART/SCK---PE5/PWMB/SCK" x="-10.16" y="-83.82" length="middle"/>
<pin name="E6/USART/RX---PE6/UARTRX/MISO" x="-10.16" y="-86.36" length="middle"/>
<pin name="E7/USART/TX---PE7/UARTTX/MOSI" x="-10.16" y="-88.9" length="middle"/>
<pin name="VCC" x="-10.16" y="22.86" length="middle"/>
<wire x1="-5.08" y1="33.02" x2="-5.08" y2="-91.44" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-91.44" x2="45.72" y2="-91.44" width="0.254" layer="94"/>
<wire x1="45.72" y1="-91.44" x2="45.72" y2="33.02" width="0.254" layer="94"/>
<wire x1="45.72" y1="33.02" x2="-5.08" y2="33.02" width="0.254" layer="94"/>
<wire x1="27.94" y1="-2.54" x2="27.94" y2="0" width="0.254" layer="94"/>
<wire x1="27.94" y1="-5.08" x2="27.94" y2="-7.62" width="0.254" layer="94"/>
<wire x1="27.94" y1="-10.16" x2="27.94" y2="-12.7" width="0.254" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="27.94" y2="-17.78" width="0.254" layer="94"/>
<wire x1="27.94" y1="-12.7" x2="25.4" y2="-12.7" width="0.254" layer="94"/>
<wire x1="25.4" y1="-10.16" x2="27.94" y2="-10.16" width="0.254" layer="94"/>
<wire x1="27.94" y1="-7.62" x2="25.4" y2="-7.62" width="0.254" layer="94"/>
<wire x1="25.4" y1="-5.08" x2="27.94" y2="-5.08" width="0.254" layer="94"/>
<wire x1="27.94" y1="-2.54" x2="25.4" y2="-2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="0" x2="27.94" y2="0" width="0.254" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="25.4" y2="-15.24" width="0.254" layer="94"/>
<wire x1="27.94" y1="-17.78" x2="25.4" y2="-17.78" width="0.254" layer="94"/>
</symbol>
<symbol name="CONN_06">
<description>&lt;h3&gt;6 Pin Connection&lt;/h3&gt;</description>
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-5.08" y="-9.906" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-5.08" y="10.668" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="SJFAB">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATK-BFPP" prefix="J">
<gates>
<gate name="G$1" symbol="ATK-BFPP" x="0" y="0"/>
</gates>
<devices>
<device name="MINIPCIE-MOTHER" package="MINI-PCIE-ATK-MOTHER">
<connects>
<connect gate="G$1" pin="+3V3" pad="15 16"/>
<connect gate="G$1" pin="+5V" pad="17 18"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="3"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="4"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="9"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="10"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="21"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="22"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="23"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="24"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="27"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="28"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="29"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="30"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="33"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="34"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="35"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="36"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="39"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="40"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="41"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="42"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="45"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="46"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="47"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="48"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="51"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="52"/>
<connect gate="G$1" pin="GND" pad="1 2 7 8 13 14 19 20 25 26 31 32 37 38 43 44 49 50 P$1"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINIPCIE-DAUGHTER" package="MINI-PCIE-ATK-DAUGHTER">
<connects>
<connect gate="G$1" pin="+3V3" pad="15 16"/>
<connect gate="G$1" pin="+5V" pad="17 18"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="3"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="4"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="9"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="10"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="21"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="22"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="23"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="24"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="27"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="28"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="29"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="30"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="33"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="34"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="35"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="36"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="39"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="40"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="41"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="42"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="45"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="46"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="47"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="48"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="51"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="52"/>
<connect gate="G$1" pin="GND" pad="1 2 7 8 13 14 19 20 25 26 31 32 37 38 43 44 49 50 P$1"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FX8-MOTHER" package="ATK-BFPP-BOTTOM-FX8-60">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$28 P$29"/>
<connect gate="G$1" pin="+5V" pad="P$26 P$27"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="P$2"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="P$3"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="P$5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="P$6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="P$8"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="P$9"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="P$11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="P$12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="P$59"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="P$58"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="P$57"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="P$56"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="P$55"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="P$54"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="P$53"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="P$52"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="P$49"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="P$48"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="P$46"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="P$45"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="P$15"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="P$16"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="P$42"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="P$40"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="P$19"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="P$20"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="P$37"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="P$36"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="P$34"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="P$33"/>
<connect gate="G$1" pin="GND" pad="P$1 P$4 P$7 P$10 P$13 P$14 P$17 P$18 P$21 P$30 P$31 P$32 P$35 P$38 P$39 P$41 P$43 P$44 P$47 P$50 P$51 P$60 P$62"/>
<connect gate="G$1" pin="VCC" pad="P$61"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FX8-DAUGHTER" package="ATK-BFPP-TOP-FX8-60">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$28 P$29"/>
<connect gate="G$1" pin="+5V" pad="P$26 P$27"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="P$2"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="P$3"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="P$5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="P$6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="P$8"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="P$9"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="P$11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="P$12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="P$59"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="P$58"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="P$57"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="P$56"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="P$55"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="P$54"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="P$53"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="P$52"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="P$49"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="P$48"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="P$46"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="P$45"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="P$15"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="P$16"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="P$42"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="P$40"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="P$19"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="P$20"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="P$37"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="P$36"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="P$34"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="P$33"/>
<connect gate="G$1" pin="GND" pad="P$1 P$4 P$7 P$10 P$13 P$14 P$17 P$18 P$21 P$30 P$31 P$32 P$35 P$38 P$39 P$41 P$43 P$44 P$47 P$50 P$51 P$60 P$62"/>
<connect gate="G$1" pin="VCC" pad="P$61"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FX8-THRU" package="ATK-BFPP-THRU-FX8-60">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$28 P$29 P$90 P$91 P$150 P$151"/>
<connect gate="G$1" pin="+5V" pad="P$26 P$27 P$88 P$89 P$148 P$149"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="P$2 P$64 P$124"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="P$3 P$65 P$125"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="P$5 P$67 P$127"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="P$6 P$68 P$128"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="P$8 P$70 P$130"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="P$9 P$71 P$131"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="P$11 P$73 P$133"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="P$12 P$74 P$134"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="P$59 P$121 P$181"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="P$58 P$120 P$180"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="P$57 P$119 P$179"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="P$56 P$118 P$178"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="P$55 P$117 P$177"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="P$54 P$116 P$176"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="P$53 P$115 P$175"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="P$52 P$114 P$174"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="P$49 P$111 P$171"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="P$48 P$110 P$170"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="P$46 P$108 P$168"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="P$45 P$107 P$167"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="P$15 P$77 P$137"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="P$16 P$78 P$138"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="P$42 P$104 P$164"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="P$40 P$102 P$162"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="P$19 P$81 P$141"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="P$20 P$82 P$142"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="P$37 P$99 P$159"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="P$36 P$98 P$158"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="P$34 P$96 P$156"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="P$33 P$95 P$155"/>
<connect gate="G$1" pin="GND" pad="P$1 P$4 P$7 P$10 P$13 P$14 P$17 P$18 P$21 P$30 P$31 P$32 P$35 P$38 P$39 P$41 P$43 P$44 P$47 P$50 P$51 P$60 P$62 P$63 P$66 P$69 P$72 P$75 P$76 P$79 P$80 P$83 P$92 P$93 P$94 P$97 P$100 P$101 P$103 P$105 P$106 P$109 P$112 P$113 P$122 P$123 P$126 P$129 P$132 P$135 P$136 P$139 P$140 P$143 P$152 P$153 P$154 P$157 P$160 P$161 P$163 P$165 P$166 P$169 P$172 P$173 P$182"/>
<connect gate="G$1" pin="VCC" pad="P$61"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST_6_PIN_HORIZONTAL" prefix="J">
<description>&lt;h3&gt;JST 6 pin horizontal connector&lt;/h3&gt;
JST-SH type.

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Here is the connector we sell at SparkFun:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="link"&gt;name&lt;/a&gt; (XXX-00000)&lt;/li&gt;
&lt;li&gt;&lt;a href="http://www.sparkfun.com/datasheets/GPS/EM406-SMDConnector-eSH.pdf"&gt;Datasheet&lt;/a&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;It was used on these SparkFun products:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="link"&gt;name&lt;/a&gt; (XXX-00000)&lt;/li&gt;
&lt;li&gt;&lt;a href="link"&gt;name&lt;/a&gt; (XXX-00000)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="A" symbol="CONN_06" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST-6-SMD-HORI-1.0MM">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XXX-00000" constant="no"/>
<attribute name="VALUE" value="BM06B-SRSS-TB" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDER_JUMPER" prefix="J">
<gates>
<gate name="G$1" symbol="SJFAB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJFAB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J2" library="connector" deviceset="ATK-BFPP" device="FX8-DAUGHTER"/>
<part name="Q1" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q2" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q3" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q4" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="U2" library="power" deviceset="DRV8701" device="QFN24"/>
<part name="R5" library="passives" deviceset="R_SHUNT" device="2512" value="50mOhm"/>
<part name="C5" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C8" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C9" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C10" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C11" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C12" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C13" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C14" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C15" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C16" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C17" library="passives" deviceset="CAP" device="0805" value="1uF"/>
<part name="C18" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C19" library="passives" deviceset="CAP" device="0805" value="1uF"/>
<part name="C20" library="passives" deviceset="CAP" device="0805" value="1uF"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R7" library="passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="R8" library="passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R6" library="passives" deviceset="RESISTOR" device="0805-RES" value="33k"/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J9" library="connector" deviceset="JST_6_PIN_HORIZONTAL" device="" value="BM06B-SRSS-TB"/>
<part name="C21" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C1" library="passives" deviceset="CAP" device="0805" value="rc"/>
<part name="C2" library="passives" deviceset="CAP" device="0805" value="rc"/>
<part name="R1" library="passives" deviceset="RESISTOR" device="0805-RES" value="rc"/>
<part name="R2" library="passives" deviceset="RESISTOR" device="0805-RES" value="rc"/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J3" library="SparkFun-Connectors" deviceset="CONN_06X2" device="NO_SILK"/>
<part name="R3" library="passives" deviceset="RESISTOR" device="0805-RES" value="vrefdivtop"/>
<part name="R4" library="passives" deviceset="RESISTOR" device="0805-RES" value="vrefdivbot"/>
<part name="R9" library="passives" deviceset="RESISTOR" device="0805-RES" value="33k"/>
<part name="C3" library="passives" deviceset="CAP" device="0805" value="rc"/>
<part name="J1" library="connector" deviceset="SOLDER_JUMPER" device=""/>
<part name="+3V5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J4" library="SparkFun-Connectors" deviceset="CONN_01" device="PTH_NO_SILK_KIT"/>
<part name="J5" library="SparkFun-Connectors" deviceset="CONN_01" device="OFFSET"/>
<part name="J6" library="SparkFun-Connectors" deviceset="CONN_01" device="OFFSET"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C4" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="274.32" y="43.18" size="1.778" layer="97">assuming no rc filter, cut trace to implement</text>
<text x="127" y="91.44" size="1.778" layer="97">assuming to use DRV8701 as current controller 
with fast PWM into this RC filter to set current
with logic into IN1 IN2 to set direction</text>
</plain>
<instances>
<instance part="J2" gate="G$1" x="78.74" y="114.3" smashed="yes"/>
<instance part="Q1" gate="1" x="342.9" y="137.16" smashed="yes">
<attribute name="VALUE" x="331.47" y="140.97" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="331.47" y="138.43" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q2" gate="1" x="342.9" y="116.84" smashed="yes">
<attribute name="VALUE" x="331.47" y="120.65" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="331.47" y="118.11" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q3" gate="1" x="327.66" y="106.68" smashed="yes">
<attribute name="VALUE" x="316.23" y="110.49" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="316.23" y="107.95" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q4" gate="1" x="327.66" y="86.36" smashed="yes">
<attribute name="VALUE" x="316.23" y="90.17" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="316.23" y="87.63" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="U2" gate="G$1" x="259.08" y="99.06" smashed="yes">
<attribute name="NAME" x="259.08" y="149.86" size="2.54" layer="95" align="center"/>
<attribute name="VALUE" x="259.08" y="48.26" size="2.54" layer="96" align="center"/>
</instance>
<instance part="R5" gate="G$1" x="330.2" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="331.5716" y="70.612" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="325.755" y="70.866" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C5" gate="G$1" x="246.38" y="172.72" smashed="yes">
<attribute name="NAME" x="247.904" y="175.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="247.904" y="170.561" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="247.904" y="168.656" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="247.904" y="166.878" size="1.27" layer="97"/>
<attribute name="TYPE" x="247.904" y="165.1" size="1.27" layer="97"/>
</instance>
<instance part="C8" gate="G$1" x="266.7" y="172.72" smashed="yes">
<attribute name="NAME" x="268.224" y="175.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="268.224" y="170.561" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="268.224" y="168.656" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="268.224" y="166.878" size="1.27" layer="97"/>
<attribute name="TYPE" x="268.224" y="165.1" size="1.27" layer="97"/>
</instance>
<instance part="C9" gate="G$1" x="276.86" y="172.72" smashed="yes">
<attribute name="NAME" x="278.384" y="175.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="278.384" y="170.561" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="278.384" y="168.656" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="278.384" y="166.878" size="1.27" layer="97"/>
<attribute name="TYPE" x="278.384" y="165.1" size="1.27" layer="97"/>
</instance>
<instance part="C10" gate="G$1" x="287.02" y="172.72" smashed="yes">
<attribute name="NAME" x="288.544" y="175.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="288.544" y="170.561" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="288.544" y="168.656" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="288.544" y="166.878" size="1.27" layer="97"/>
<attribute name="TYPE" x="288.544" y="165.1" size="1.27" layer="97"/>
</instance>
<instance part="C11" gate="G$1" x="297.18" y="172.72" smashed="yes">
<attribute name="NAME" x="298.704" y="175.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.704" y="170.561" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="298.704" y="168.656" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="298.704" y="166.878" size="1.27" layer="97"/>
<attribute name="TYPE" x="298.704" y="165.1" size="1.27" layer="97"/>
</instance>
<instance part="C12" gate="G$1" x="307.34" y="172.72" smashed="yes">
<attribute name="NAME" x="308.864" y="175.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="308.864" y="170.561" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="308.864" y="168.656" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="308.864" y="166.878" size="1.27" layer="97"/>
<attribute name="TYPE" x="308.864" y="165.1" size="1.27" layer="97"/>
</instance>
<instance part="C13" gate="G$1" x="317.5" y="172.72" smashed="yes">
<attribute name="NAME" x="319.024" y="175.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="319.024" y="170.561" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="319.024" y="168.656" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="319.024" y="166.878" size="1.27" layer="97"/>
<attribute name="TYPE" x="319.024" y="165.1" size="1.27" layer="97"/>
</instance>
<instance part="C14" gate="G$1" x="327.66" y="172.72" smashed="yes">
<attribute name="NAME" x="329.184" y="175.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="329.184" y="170.561" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="329.184" y="168.656" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="329.184" y="166.878" size="1.27" layer="97"/>
<attribute name="TYPE" x="329.184" y="165.1" size="1.27" layer="97"/>
</instance>
<instance part="C15" gate="G$1" x="337.82" y="172.72" smashed="yes">
<attribute name="NAME" x="339.344" y="175.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="339.344" y="170.561" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="339.344" y="168.656" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="339.344" y="166.878" size="1.27" layer="97"/>
<attribute name="TYPE" x="339.344" y="165.1" size="1.27" layer="97"/>
</instance>
<instance part="C16" gate="G$1" x="330.2" y="147.32" smashed="yes">
<attribute name="NAME" x="331.724" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="331.724" y="145.161" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="331.724" y="143.256" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="331.724" y="141.478" size="1.27" layer="97"/>
<attribute name="TYPE" x="331.724" y="139.7" size="1.27" layer="97"/>
</instance>
<instance part="GND11" gate="1" x="368.3" y="170.18" smashed="yes" rot="R90">
<attribute name="VALUE" x="370.84" y="167.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND12" gate="1" x="330.2" y="157.48" smashed="yes" rot="R180">
<attribute name="VALUE" x="332.74" y="160.02" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND13" gate="1" x="231.14" y="53.34" smashed="yes" rot="R270">
<attribute name="VALUE" x="228.6" y="55.88" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C17" gate="G$1" x="236.22" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="233.299" y="146.304" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="238.379" y="146.304" size="1.778" layer="96" rot="R90"/>
<attribute name="PACKAGE" x="240.284" y="146.304" size="1.27" layer="97" rot="R90"/>
<attribute name="VOLTAGE" x="242.062" y="146.304" size="1.27" layer="97" rot="R90"/>
<attribute name="TYPE" x="243.84" y="146.304" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="C18" gate="G$1" x="231.14" y="132.08" smashed="yes">
<attribute name="NAME" x="232.664" y="135.001" size="1.778" layer="95"/>
<attribute name="VALUE" x="232.664" y="129.921" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="232.664" y="128.016" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="232.664" y="126.238" size="1.27" layer="97"/>
<attribute name="TYPE" x="232.664" y="124.46" size="1.27" layer="97"/>
</instance>
<instance part="C19" gate="G$1" x="236.22" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="233.299" y="123.444" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="238.379" y="123.444" size="1.778" layer="96" rot="R90"/>
<attribute name="PACKAGE" x="240.284" y="123.444" size="1.27" layer="97" rot="R90"/>
<attribute name="VOLTAGE" x="242.062" y="123.444" size="1.27" layer="97" rot="R90"/>
<attribute name="TYPE" x="243.84" y="123.444" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="C20" gate="G$1" x="236.22" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="233.299" y="115.824" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="238.379" y="115.824" size="1.778" layer="96" rot="R90"/>
<attribute name="PACKAGE" x="240.284" y="115.824" size="1.27" layer="97" rot="R90"/>
<attribute name="VOLTAGE" x="242.062" y="115.824" size="1.27" layer="97" rot="R90"/>
<attribute name="TYPE" x="243.84" y="115.824" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="GND14" gate="1" x="226.06" y="121.92" smashed="yes" rot="R270">
<attribute name="VALUE" x="223.52" y="124.46" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND15" gate="1" x="226.06" y="114.3" smashed="yes" rot="R270">
<attribute name="VALUE" x="223.52" y="116.84" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R7" gate="G$1" x="215.9" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="219.71" y="67.0814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="219.71" y="71.882" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="219.71" y="75.438" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="219.71" y="73.66" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="R8" gate="G$1" x="203.2" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="207.01" y="62.0014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="207.01" y="66.802" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="207.01" y="70.358" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="207.01" y="68.58" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="+3V1" gate="G$1" x="190.5" y="66.04" smashed="yes" rot="R90">
<attribute name="VALUE" x="195.58" y="63.5" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R6" gate="G$1" x="220.98" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="224.79" y="87.4014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="224.79" y="92.202" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="224.79" y="95.758" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="224.79" y="93.98" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="GND16" gate="1" x="210.82" y="88.9" smashed="yes" rot="R270">
<attribute name="VALUE" x="208.28" y="91.44" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J9" gate="A" x="12.7" y="25.4" smashed="yes">
<attribute name="VALUE" x="7.62" y="15.494" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="7.62" y="36.068" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="C21" gate="G$1" x="38.1" y="17.78" smashed="yes">
<attribute name="NAME" x="39.624" y="20.701" size="1.778" layer="95"/>
<attribute name="VALUE" x="39.624" y="15.621" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="39.624" y="13.716" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="39.624" y="11.938" size="1.27" layer="97"/>
<attribute name="TYPE" x="39.624" y="10.16" size="1.27" layer="97"/>
</instance>
<instance part="GND17" gate="1" x="38.1" y="10.16" smashed="yes">
<attribute name="VALUE" x="35.56" y="7.62" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="330.2" y="50.8" smashed="yes">
<attribute name="VALUE" x="327.66" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="+3V2" gate="G$1" x="48.26" y="129.54" smashed="yes" rot="R90">
<attribute name="VALUE" x="53.34" y="127" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+3V3" gate="G$1" x="48.26" y="22.86" smashed="yes" rot="R270">
<attribute name="VALUE" x="43.18" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="33.02" y="66.04" smashed="yes" rot="R90">
<attribute name="VALUE" x="35.56" y="63.5" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V4" gate="G$1" x="15.24" y="93.98" smashed="yes" rot="R270">
<attribute name="VALUE" x="10.16" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="48.26" y="144.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="45.72" y="147.32" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C1" gate="G$1" x="289.56" y="68.58" smashed="yes">
<attribute name="NAME" x="291.084" y="71.501" size="1.778" layer="95"/>
<attribute name="VALUE" x="291.084" y="66.421" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="291.084" y="64.516" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="291.084" y="62.738" size="1.27" layer="97"/>
<attribute name="TYPE" x="291.084" y="60.96" size="1.27" layer="97"/>
</instance>
<instance part="C2" gate="G$1" x="289.56" y="55.88" smashed="yes">
<attribute name="NAME" x="291.084" y="58.801" size="1.778" layer="95"/>
<attribute name="VALUE" x="291.084" y="53.721" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="291.084" y="51.816" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="291.084" y="50.038" size="1.27" layer="97"/>
<attribute name="TYPE" x="291.084" y="48.26" size="1.27" layer="97"/>
</instance>
<instance part="R1" gate="G$1" x="302.26" y="73.66" smashed="yes">
<attribute name="NAME" x="298.45" y="75.1586" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="70.358" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="66.802" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="68.58" size="1.27" layer="97"/>
</instance>
<instance part="R2" gate="G$1" x="302.26" y="60.96" smashed="yes">
<attribute name="NAME" x="298.45" y="62.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="57.658" size="1.778" layer="96"/>
<attribute name="PRECISION" x="298.45" y="54.102" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="298.45" y="55.88" size="1.27" layer="97"/>
</instance>
<instance part="GND2" gate="1" x="281.94" y="66.04" smashed="yes" rot="R270">
<attribute name="VALUE" x="279.4" y="68.58" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND3" gate="1" x="281.94" y="53.34" smashed="yes" rot="R270">
<attribute name="VALUE" x="279.4" y="55.88" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J3" gate="G$1" x="15.24" y="63.5" smashed="yes">
<attribute name="VALUE" x="13.97" y="58.674" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="13.97" y="79.248" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="R3" gate="G$1" x="185.42" y="88.9" smashed="yes" rot="R270">
<attribute name="NAME" x="186.9186" y="92.71" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="182.118" y="92.71" size="1.778" layer="96" rot="R270"/>
<attribute name="PRECISION" x="178.562" y="92.71" size="1.27" layer="97" rot="R270"/>
<attribute name="PACKAGE" x="180.34" y="92.71" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="R4" gate="G$1" x="185.42" y="68.58" smashed="yes" rot="R270">
<attribute name="NAME" x="186.9186" y="72.39" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="182.118" y="72.39" size="1.778" layer="96" rot="R270"/>
<attribute name="PRECISION" x="178.562" y="72.39" size="1.27" layer="97" rot="R270"/>
<attribute name="PACKAGE" x="180.34" y="72.39" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="R9" gate="G$1" x="147.32" y="78.74" smashed="yes">
<attribute name="NAME" x="143.51" y="80.2386" size="1.778" layer="95"/>
<attribute name="VALUE" x="143.51" y="75.438" size="1.778" layer="96"/>
<attribute name="PRECISION" x="143.51" y="71.882" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="143.51" y="73.66" size="1.27" layer="97"/>
</instance>
<instance part="C3" gate="G$1" x="157.48" y="73.66" smashed="yes">
<attribute name="NAME" x="159.004" y="76.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="159.004" y="71.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="159.004" y="69.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="159.004" y="67.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="159.004" y="66.04" size="1.27" layer="97"/>
</instance>
<instance part="J1" gate="G$1" x="170.18" y="78.74" smashed="yes">
<attribute name="NAME" x="167.64" y="81.28" size="1.778" layer="95"/>
<attribute name="VALUE" x="167.64" y="74.93" size="1.778" layer="96"/>
</instance>
<instance part="+3V5" gate="G$1" x="185.42" y="106.68" smashed="yes">
<attribute name="VALUE" x="182.88" y="101.6" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND4" gate="1" x="185.42" y="55.88" smashed="yes">
<attribute name="VALUE" x="182.88" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="157.48" y="55.88" smashed="yes">
<attribute name="VALUE" x="154.94" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="J4" gate="G$1" x="195.58" y="86.36" smashed="yes" rot="R270">
<attribute name="VALUE" x="190.754" y="88.9" size="1.778" layer="96" font="vector" rot="R270"/>
<attribute name="NAME" x="198.628" y="88.9" size="1.778" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="J5" gate="G$1" x="373.38" y="116.84" smashed="yes" rot="R180">
<attribute name="VALUE" x="375.92" y="121.666" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="375.92" y="113.792" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J6" gate="G$1" x="373.38" y="101.6" smashed="yes" rot="R180">
<attribute name="VALUE" x="375.92" y="106.426" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="375.92" y="98.552" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="GND6" gate="1" x="33.02" y="73.66" smashed="yes" rot="R90">
<attribute name="VALUE" x="35.56" y="71.12" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V6" gate="G$1" x="40.64" y="76.2" smashed="yes" rot="R270">
<attribute name="VALUE" x="35.56" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="+3V7" gate="G$1" x="40.64" y="63.5" smashed="yes" rot="R270">
<attribute name="VALUE" x="35.56" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="+3V8" gate="G$1" x="-15.24" y="66.04" smashed="yes" rot="R90">
<attribute name="VALUE" x="-10.16" y="63.5" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND7" gate="1" x="2.54" y="50.8" smashed="yes">
<attribute name="VALUE" x="0" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="2.54" y="60.96" smashed="yes">
<attribute name="NAME" x="4.064" y="63.881" size="1.778" layer="95"/>
<attribute name="VALUE" x="4.064" y="58.801" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="4.064" y="56.896" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="4.064" y="55.118" size="1.27" layer="97"/>
<attribute name="TYPE" x="4.064" y="53.34" size="1.27" layer="97"/>
</instance>
<instance part="P+1" gate="VCC" x="226.06" y="144.78" smashed="yes" rot="R90">
<attribute name="VALUE" x="228.6" y="142.24" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+3" gate="VCC" x="345.44" y="152.4" smashed="yes">
<attribute name="VALUE" x="342.9" y="149.86" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+4" gate="VCC" x="368.3" y="177.8" smashed="yes" rot="R270">
<attribute name="VALUE" x="365.76" y="180.34" size="1.778" layer="96"/>
</instance>
<instance part="P+5" gate="VCC" x="48.26" y="137.16" smashed="yes" rot="R90">
<attribute name="VALUE" x="50.8" y="134.62" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GH2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="GH2"/>
<pinref part="Q3" gate="1" pin="G"/>
<wire x1="276.86" y1="104.14" x2="325.12" y2="104.14" width="0.1524" layer="91"/>
<label x="279.4" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="SH2" class="0">
<segment>
<pinref part="Q4" gate="1" pin="D"/>
<pinref part="Q3" gate="1" pin="S"/>
<wire x1="330.2" y1="91.44" x2="330.2" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SH2"/>
<wire x1="330.2" y1="93.98" x2="330.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="276.86" y1="93.98" x2="330.2" y2="93.98" width="0.1524" layer="91"/>
<junction x="330.2" y="93.98"/>
<wire x1="330.2" y1="93.98" x2="360.68" y2="93.98" width="0.1524" layer="91"/>
<wire x1="360.68" y1="93.98" x2="360.68" y2="101.6" width="0.1524" layer="91"/>
<label x="279.4" y="93.98" size="1.778" layer="95"/>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="365.76" y1="101.6" x2="360.68" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GL2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="GL2"/>
<pinref part="Q4" gate="1" pin="G"/>
<wire x1="276.86" y1="83.82" x2="325.12" y2="83.82" width="0.1524" layer="91"/>
<label x="279.4" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="GL1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="GL1"/>
<pinref part="Q2" gate="1" pin="G"/>
<wire x1="276.86" y1="114.3" x2="340.36" y2="114.3" width="0.1524" layer="91"/>
<label x="279.4" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="246.38" y1="170.18" x2="266.7" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="266.7" y1="170.18" x2="276.86" y2="170.18" width="0.1524" layer="91"/>
<junction x="266.7" y="170.18"/>
<wire x1="276.86" y1="170.18" x2="287.02" y2="170.18" width="0.1524" layer="91"/>
<junction x="276.86" y="170.18"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="287.02" y1="170.18" x2="297.18" y2="170.18" width="0.1524" layer="91"/>
<junction x="287.02" y="170.18"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="297.18" y1="170.18" x2="307.34" y2="170.18" width="0.1524" layer="91"/>
<junction x="297.18" y="170.18"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="307.34" y1="170.18" x2="317.5" y2="170.18" width="0.1524" layer="91"/>
<junction x="307.34" y="170.18"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="317.5" y1="170.18" x2="327.66" y2="170.18" width="0.1524" layer="91"/>
<junction x="317.5" y="170.18"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="327.66" y1="170.18" x2="337.82" y2="170.18" width="0.1524" layer="91"/>
<junction x="327.66" y="170.18"/>
<pinref part="C15" gate="G$1" pin="2"/>
<junction x="337.82" y="170.18"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="337.82" y1="170.18" x2="365.76" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="330.2" y1="152.4" x2="330.2" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="233.68" y1="53.34" x2="241.3" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="228.6" y1="121.92" x2="231.14" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="228.6" y1="114.3" x2="231.14" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="213.36" y1="88.9" x2="215.9" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="330.2" y1="53.34" x2="330.2" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="A"/>
<wire x1="330.2" y1="60.96" x2="330.2" y2="63.5" width="0.1524" layer="91"/>
<junction x="330.2" y="60.96"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J9" gate="A" pin="1"/>
<wire x1="17.78" y1="20.32" x2="30.48" y2="20.32" width="0.1524" layer="91"/>
<wire x1="30.48" y1="20.32" x2="30.48" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="30.48" y1="15.24" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="38.1" y1="15.24" x2="38.1" y2="12.7" width="0.1524" layer="91"/>
<junction x="38.1" y="15.24"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="J2" gate="G$1" pin="GND"/>
<wire x1="50.8" y1="144.78" x2="68.58" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="284.48" y1="66.04" x2="289.56" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="284.48" y1="53.34" x2="289.56" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="185.42" y1="58.42" x2="185.42" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="157.48" y1="58.42" x2="157.48" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="2.54" y1="58.42" x2="2.54" y2="53.34" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="11"/>
<wire x1="10.16" y1="63.5" x2="10.16" y2="58.42" width="0.1524" layer="91"/>
<wire x1="10.16" y1="58.42" x2="2.54" y2="58.42" width="0.1524" layer="91"/>
<junction x="2.54" y="58.42"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="4"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="25.4" y1="73.66" x2="30.48" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="10"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="25.4" y1="66.04" x2="30.48" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GH1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="GH1"/>
<pinref part="Q1" gate="1" pin="G"/>
<wire x1="276.86" y1="134.62" x2="340.36" y2="134.62" width="0.1524" layer="91"/>
<label x="279.4" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="SNSOUT" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="SNSOUT"/>
<wire x1="220.98" y1="68.58" x2="241.3" y2="68.58" width="0.1524" layer="91"/>
<label x="226.06" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT"/>
<wire x1="68.58" y1="96.52" x2="50.8" y2="96.52" width="0.1524" layer="91"/>
<label x="50.8" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="IDRIVE" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IDRIVE"/>
<wire x1="241.3" y1="88.9" x2="226.06" y2="88.9" width="0.1524" layer="91"/>
<label x="226.06" y="88.9" size="1.778" layer="95"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SH1" class="0">
<segment>
<pinref part="Q1" gate="1" pin="S"/>
<pinref part="Q2" gate="1" pin="D"/>
<wire x1="345.44" y1="132.08" x2="345.44" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SH1"/>
<wire x1="345.44" y1="124.46" x2="345.44" y2="121.92" width="0.1524" layer="91"/>
<wire x1="276.86" y1="124.46" x2="345.44" y2="124.46" width="0.1524" layer="91"/>
<junction x="345.44" y="124.46"/>
<wire x1="345.44" y1="124.46" x2="360.68" y2="124.46" width="0.1524" layer="91"/>
<wire x1="360.68" y1="124.46" x2="360.68" y2="116.84" width="0.1524" layer="91"/>
<label x="279.4" y="124.46" size="1.778" layer="95"/>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="360.68" y1="116.84" x2="365.76" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SHNTTP" class="0">
<segment>
<pinref part="Q4" gate="1" pin="S"/>
<wire x1="330.2" y1="73.66" x2="330.2" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="B"/>
<wire x1="330.2" y1="76.2" x2="330.2" y2="81.28" width="0.1524" layer="91"/>
<wire x1="330.2" y1="76.2" x2="345.44" y2="76.2" width="0.1524" layer="91"/>
<junction x="330.2" y="76.2"/>
<pinref part="Q2" gate="1" pin="S"/>
<wire x1="345.44" y1="76.2" x2="345.44" y2="111.76" width="0.1524" layer="91"/>
<label x="345.44" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="SP" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SP"/>
<wire x1="276.86" y1="73.66" x2="289.56" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="289.56" y1="73.66" x2="297.18" y2="73.66" width="0.1524" layer="91"/>
<junction x="289.56" y="73.66"/>
<label x="279.4" y="73.66" size="1.778" layer="95"/>
<wire x1="297.18" y1="73.66" x2="297.18" y2="71.12" width="0.1524" layer="91"/>
<junction x="297.18" y="73.66"/>
<wire x1="297.18" y1="71.12" x2="307.34" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="R5" gate="G$1" pin="SB"/>
<wire x1="307.34" y1="73.66" x2="327.66" y2="73.66" width="0.1524" layer="91"/>
<label x="309.88" y="73.66" size="1.778" layer="95"/>
<wire x1="307.34" y1="71.12" x2="307.34" y2="73.66" width="0.1524" layer="91"/>
<junction x="307.34" y="73.66"/>
</segment>
</net>
<net name="SN" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SN"/>
<wire x1="276.86" y1="60.96" x2="289.56" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="297.18" y1="60.96" x2="289.56" y2="60.96" width="0.1524" layer="91"/>
<junction x="289.56" y="60.96"/>
<label x="279.4" y="60.96" size="1.778" layer="95"/>
<wire x1="297.18" y1="60.96" x2="297.18" y2="58.42" width="0.1524" layer="91"/>
<junction x="297.18" y="60.96"/>
<wire x1="297.18" y1="58.42" x2="307.34" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="SA"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="327.66" y1="60.96" x2="307.34" y2="60.96" width="0.1524" layer="91"/>
<label x="309.88" y="60.96" size="1.778" layer="95"/>
<wire x1="307.34" y1="58.42" x2="307.34" y2="60.96" width="0.1524" layer="91"/>
<junction x="307.34" y="60.96"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="VCP"/>
<wire x1="238.76" y1="144.78" x2="241.3" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="CPH"/>
<wire x1="231.14" y1="137.16" x2="241.3" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="CPL"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="241.3" y1="129.54" x2="231.14" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="AVDD"/>
<wire x1="238.76" y1="121.92" x2="241.3" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="DVDD"/>
<wire x1="238.76" y1="114.3" x2="241.3" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NFAULT" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="NFAULT"/>
<wire x1="208.28" y1="63.5" x2="241.3" y2="63.5" width="0.1524" layer="91"/>
<label x="226.06" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT"/>
<wire x1="68.58" y1="99.06" x2="50.8" y2="99.06" width="0.1524" layer="91"/>
<label x="50.8" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="J9" gate="A" pin="2"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="17.78" y1="22.86" x2="38.1" y2="22.86" width="0.1524" layer="91"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="38.1" y1="22.86" x2="45.72" y2="22.86" width="0.1524" layer="91"/>
<junction x="38.1" y="22.86"/>
</segment>
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="J2" gate="G$1" pin="+3V3"/>
<wire x1="50.8" y1="129.54" x2="68.58" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="193.04" y1="66.04" x2="195.58" y2="66.04" width="0.1524" layer="91"/>
<wire x1="195.58" y1="66.04" x2="195.58" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="195.58" y1="68.58" x2="210.82" y2="68.58" width="0.1524" layer="91"/>
<wire x1="195.58" y1="66.04" x2="195.58" y2="63.5" width="0.1524" layer="91"/>
<junction x="195.58" y="66.04"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="195.58" y1="63.5" x2="198.12" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="185.42" y1="93.98" x2="185.42" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="9"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="10.16" y1="66.04" x2="2.54" y2="66.04" width="0.1524" layer="91"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="2.54" y1="66.04" x2="-12.7" y2="66.04" width="0.1524" layer="91"/>
<junction x="2.54" y="66.04"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="76.2" x2="38.1" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="12"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="63.5" x2="38.1" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SENSE" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SO"/>
<wire x1="241.3" y1="73.66" x2="226.06" y2="73.66" width="0.1524" layer="91"/>
<label x="226.06" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="A1/ADC---PB1/ADC"/>
<wire x1="68.58" y1="111.76" x2="50.8" y2="111.76" width="0.1524" layer="91"/>
<label x="50.8" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IN1"/>
<wire x1="241.3" y1="104.14" x2="226.06" y2="104.14" width="0.1524" layer="91"/>
<label x="226.06" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX"/>
<wire x1="68.58" y1="76.2" x2="50.8" y2="76.2" width="0.1524" layer="91"/>
<label x="50.8" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IN2"/>
<wire x1="241.3" y1="99.06" x2="226.06" y2="99.06" width="0.1524" layer="91"/>
<label x="226.06" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX"/>
<wire x1="68.58" y1="73.66" x2="50.8" y2="73.66" width="0.1524" layer="91"/>
<label x="50.8" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="NSLEEP" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="NSLEEP"/>
<wire x1="241.3" y1="93.98" x2="226.06" y2="93.98" width="0.1524" layer="91"/>
<label x="226.06" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN"/>
<wire x1="68.58" y1="81.28" x2="50.8" y2="81.28" width="0.1524" layer="91"/>
<label x="50.8" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_MISO" class="0">
<segment>
<pinref part="J9" gate="A" pin="3"/>
<wire x1="17.78" y1="25.4" x2="30.48" y2="25.4" width="0.1524" layer="91"/>
<label x="17.78" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO"/>
<wire x1="68.58" y1="27.94" x2="50.8" y2="27.94" width="0.1524" layer="91"/>
<label x="50.8" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_CSN" class="0">
<segment>
<pinref part="J9" gate="A" pin="5"/>
<wire x1="17.78" y1="30.48" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
<label x="17.78" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN"/>
<wire x1="68.58" y1="33.02" x2="50.8" y2="33.02" width="0.1524" layer="91"/>
<label x="50.8" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_MOSI" class="0">
<segment>
<pinref part="J9" gate="A" pin="6"/>
<wire x1="17.78" y1="33.02" x2="30.48" y2="33.02" width="0.1524" layer="91"/>
<label x="17.78" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI"/>
<wire x1="68.58" y1="25.4" x2="50.8" y2="25.4" width="0.1524" layer="91"/>
<label x="50.8" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_SCK" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK"/>
<wire x1="68.58" y1="30.48" x2="50.8" y2="30.48" width="0.1524" layer="91"/>
<label x="50.8" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="A" pin="4"/>
<wire x1="17.78" y1="27.94" x2="30.48" y2="27.94" width="0.1524" layer="91"/>
<label x="17.78" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="DRV_VREF" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="185.42" y1="83.82" x2="185.42" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="185.42" y1="78.74" x2="185.42" y2="73.66" width="0.1524" layer="91"/>
<wire x1="175.26" y1="78.74" x2="185.42" y2="78.74" width="0.1524" layer="91"/>
<junction x="185.42" y="78.74"/>
<pinref part="U2" gate="G$1" pin="VREF"/>
<wire x1="185.42" y1="78.74" x2="195.58" y2="78.74" width="0.1524" layer="91"/>
<label x="226.06" y="78.74" size="1.778" layer="95"/>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="195.58" y1="78.74" x2="241.3" y2="78.74" width="0.1524" layer="91"/>
<wire x1="152.4" y1="78.74" x2="157.48" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="1"/>
<junction x="157.48" y="78.74"/>
<wire x1="157.48" y1="78.74" x2="165.1" y2="78.74" width="0.1524" layer="91"/>
<wire x1="165.1" y1="78.74" x2="165.1" y2="73.66" width="0.1524" layer="91"/>
<junction x="165.1" y="78.74"/>
<wire x1="165.1" y1="73.66" x2="175.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="175.26" y1="73.66" x2="175.26" y2="78.74" width="0.1524" layer="91"/>
<junction x="175.26" y="78.74"/>
<pinref part="J4" gate="G$1" pin="1"/>
<junction x="195.58" y="78.74"/>
</segment>
</net>
<net name="PWM_VREF" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="D4/PWMELS---PD4"/>
<wire x1="68.58" y1="55.88" x2="50.8" y2="55.88" width="0.1524" layer="91"/>
<label x="50.8" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="142.24" y1="78.74" x2="127" y2="78.74" width="0.1524" layer="91"/>
<label x="127" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="OPT/C0" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="10.16" y1="76.2" x2="-12.7" y2="76.2" width="0.1524" layer="91"/>
<label x="-12.7" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA"/>
<wire x1="68.58" y1="91.44" x2="50.8" y2="91.44" width="0.1524" layer="91"/>
<label x="50.8" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="OPT/C1" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="3"/>
<wire x1="10.16" y1="73.66" x2="-12.7" y2="73.66" width="0.1524" layer="91"/>
<label x="-12.7" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL"/>
<wire x1="50.8" y1="88.9" x2="68.58" y2="88.9" width="0.1524" layer="91"/>
<label x="50.8" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="OPT/C2" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="5"/>
<wire x1="10.16" y1="71.12" x2="-12.7" y2="71.12" width="0.1524" layer="91"/>
<label x="-12.7" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX"/>
<wire x1="68.58" y1="86.36" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<label x="50.8" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="OPT/C3" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="7"/>
<wire x1="10.16" y1="68.58" x2="-12.7" y2="68.58" width="0.1524" layer="91"/>
<label x="-12.7" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX"/>
<wire x1="50.8" y1="83.82" x2="68.58" y2="83.82" width="0.1524" layer="91"/>
<label x="50.8" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="OPT/A4" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="6"/>
<wire x1="25.4" y1="71.12" x2="38.1" y2="71.12" width="0.1524" layer="91"/>
<label x="27.94" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="A4/ADC---PB4/ADC"/>
<wire x1="68.58" y1="104.14" x2="50.8" y2="104.14" width="0.1524" layer="91"/>
<label x="50.8" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="OPT/A5" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="8"/>
<wire x1="25.4" y1="68.58" x2="38.1" y2="68.58" width="0.1524" layer="91"/>
<label x="27.94" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="A5/ADC---PB5/ADC"/>
<wire x1="68.58" y1="101.6" x2="50.8" y2="101.6" width="0.1524" layer="91"/>
<label x="50.8" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="228.6" y1="144.78" x2="231.14" y2="144.78" width="0.1524" layer="91"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VM"/>
<wire x1="276.86" y1="144.78" x2="330.2" y2="144.78" width="0.1524" layer="91"/>
<pinref part="Q1" gate="1" pin="D"/>
<wire x1="330.2" y1="144.78" x2="345.44" y2="144.78" width="0.1524" layer="91"/>
<wire x1="345.44" y1="144.78" x2="345.44" y2="142.24" width="0.1524" layer="91"/>
<wire x1="345.44" y1="144.78" x2="345.44" y2="149.86" width="0.1524" layer="91"/>
<junction x="345.44" y="144.78"/>
<pinref part="Q3" gate="1" pin="D"/>
<wire x1="330.2" y1="111.76" x2="330.2" y2="144.78" width="0.1524" layer="91"/>
<junction x="330.2" y="144.78"/>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="P+3" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="246.38" y1="177.8" x2="266.7" y2="177.8" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="266.7" y1="177.8" x2="276.86" y2="177.8" width="0.1524" layer="91"/>
<junction x="266.7" y="177.8"/>
<wire x1="276.86" y1="177.8" x2="287.02" y2="177.8" width="0.1524" layer="91"/>
<junction x="276.86" y="177.8"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="287.02" y1="177.8" x2="297.18" y2="177.8" width="0.1524" layer="91"/>
<junction x="287.02" y="177.8"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="297.18" y1="177.8" x2="307.34" y2="177.8" width="0.1524" layer="91"/>
<junction x="297.18" y="177.8"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="307.34" y1="177.8" x2="317.5" y2="177.8" width="0.1524" layer="91"/>
<junction x="307.34" y="177.8"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="317.5" y1="177.8" x2="327.66" y2="177.8" width="0.1524" layer="91"/>
<junction x="317.5" y="177.8"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="327.66" y1="177.8" x2="337.82" y2="177.8" width="0.1524" layer="91"/>
<junction x="327.66" y="177.8"/>
<pinref part="C15" gate="G$1" pin="1"/>
<junction x="337.82" y="177.8"/>
<wire x1="337.82" y1="177.8" x2="365.76" y2="177.8" width="0.1524" layer="91"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="VCC"/>
<wire x1="50.8" y1="137.16" x2="68.58" y2="137.16" width="0.1524" layer="91"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
